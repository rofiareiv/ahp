-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 21, 2016 at 09:57 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbahp-abusairi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblalternatif`
--

CREATE TABLE IF NOT EXISTS `tblalternatif` (
  `idalternatif` int(30) NOT NULL AUTO_INCREMENT,
  `nama_alternatif` varchar(100) NOT NULL,
  `nama_alias` varchar(50) NOT NULL,
  PRIMARY KEY (`idalternatif`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tblalternatif`
--

INSERT INTO `tblalternatif` (`idalternatif`, `nama_alternatif`, `nama_alias`) VALUES
(1, 'PT PRO ROLL INTERNATIONAL', 'PRI'),
(2, 'PT LARIS MANIS', 'LM'),
(3, 'PT ANGRAH PHARMINDO LESTARI', 'APL'),
(4, 'PT DANLIRIS SUKAHARJO', 'DS'),
(5, 'PT DJARUM', 'JRM'),
(7, 'PT BATAMTEX', 'BT');

-- --------------------------------------------------------

--
-- Table structure for table `tblbobotalternatif`
--

CREATE TABLE IF NOT EXISTS `tblbobotalternatif` (
  `idbobot` int(30) NOT NULL AUTO_INCREMENT,
  `idalternatif` int(30) NOT NULL,
  `nama_alias` varchar(20) NOT NULL,
  `PRI` double NOT NULL,
  `LM` double NOT NULL,
  `APL` double NOT NULL,
  `DS` double NOT NULL,
  `JRM` double NOT NULL,
  `BT` double NOT NULL,
  `tanda` int(10) NOT NULL,
  PRIMARY KEY (`idbobot`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=113 ;

--
-- Dumping data for table `tblbobotalternatif`
--

INSERT INTO `tblbobotalternatif` (`idbobot`, `idalternatif`, `nama_alias`, `PRI`, `LM`, `APL`, `DS`, `JRM`, `BT`, `tanda`) VALUES
(78, 1, 'PRI', 1, 3, 5, 4, 2, 7, 2),
(79, 2, 'LM', 0.333, 1, 4, 2, 3, 5, 2),
(80, 3, 'APL', 0.2, 0.25, 1, 3, 7, 6, 2),
(81, 4, 'DS', 0.25, 0.5, 0.333, 1, 5, 4, 2),
(82, 5, 'JRM', 0.5, 0.333, 0.143, 0.2, 1, 2, 2),
(83, 7, 'BT', 0.143, 0.2, 0.167, 0.25, 0.5, 1, 2),
(84, 7, 'jumlah', 2.426, 5.283, 10.643, 10.45, 18.5, 25, 2),
(85, 1, 'PRI', 1, 5, 0.5, 4, 3, 2, 3),
(86, 2, 'LM', 0.2, 1, 7, 5, 6, 3, 3),
(87, 3, 'APL', 2, 0.143, 1, 6, 2, 4, 3),
(88, 4, 'DS', 0.25, 0.2, 0.167, 1, 4, 5, 3),
(89, 5, 'JRM', 0.333, 0.167, 0.5, 0.25, 1, 7, 3),
(90, 7, 'BT', 0.5, 0.333, 0.25, 0.2, 0.143, 1, 3),
(91, 7, 'jumlah', 4.283, 6.843, 9.417, 16.45, 16.143, 22, 3),
(92, 1, 'PRI', 1, 6, 4, 2, 5, 3, 4),
(93, 2, 'LM', 0.167, 1, 5, 3, 7, 2, 4),
(94, 3, 'APL', 0.25, 0.2, 1, 4, 2, 5, 4),
(95, 4, 'DS', 0.5, 0.333, 0.25, 1, 6, 4, 4),
(96, 5, 'JRM', 0.2, 0.143, 0.5, 0.167, 1, 7, 4),
(97, 7, 'BT', 0.333, 0.5, 0.2, 0.25, 0.143, 1, 4),
(98, 7, 'jumlah', 2.45, 8.176, 10.95, 10.417, 21.143, 22, 4),
(99, 1, 'PRI', 1, 7, 4, 3, 5, 2, 5),
(100, 2, 'LM', 0.143, 1, 5, 6, 4, 3, 5),
(101, 3, 'APL', 0.25, 0.2, 1, 2, 7, 6, 5),
(102, 4, 'DS', 0.333, 0.167, 0.5, 1, 3, 5, 5),
(103, 5, 'JRM', 0.2, 0.25, 0.143, 0.333, 1, 4, 5),
(104, 7, 'BT', 0.5, 0.333, 0.167, 0.2, 0.25, 1, 5),
(105, 7, 'jumlah', 2.426, 8.95, 10.81, 12.533, 20.25, 21, 5),
(106, 1, 'PRI', 1, 5, 2, 3, 4, 6, 1),
(107, 2, 'LM', 0.2, 1, 6, 2, 3, 4, 1),
(108, 3, 'APL', 0.5, 0.167, 1, 4, 2, 3, 1),
(109, 4, 'DS', 0.333, 0.5, 0.25, 1, 5, 2, 1),
(110, 5, 'JRM', 0.25, 0.333, 0.5, 0.2, 1, 5, 1),
(111, 7, 'BT', 0.167, 0.25, 0.333, 0.5, 0.2, 1, 1),
(112, 7, 'jumlah', 2.45, 7.25, 10.083, 10.7, 15.2, 21, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblbobotkriteria`
--

CREATE TABLE IF NOT EXISTS `tblbobotkriteria` (
  `idbobot` int(30) NOT NULL AUTO_INCREMENT,
  `idkriteria` int(30) NOT NULL,
  `simbol` varchar(20) NOT NULL,
  `A1` double NOT NULL,
  `A2` double NOT NULL,
  `A3` double NOT NULL,
  `A4` double NOT NULL,
  `A5` double NOT NULL,
  PRIMARY KEY (`idbobot`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tblbobotkriteria`
--

INSERT INTO `tblbobotkriteria` (`idbobot`, `idkriteria`, `simbol`, `A1`, `A2`, `A3`, `A4`, `A5`) VALUES
(1, 1, 'A1', 1, 2, 3, 4, 5),
(2, 2, 'A2', 0.5, 1, 2, 3, 4),
(3, 3, 'A3', 0.333, 0.5, 1, 4, 2),
(4, 4, 'A4', 0.25, 0.333, 0.25, 1, 3),
(5, 5, 'A5', 0.2, 0.25, 0.5, 0.333, 1),
(6, 6, 'jumlah', 2.283, 4.083, 6.75, 12.333, 15);

-- --------------------------------------------------------

--
-- Table structure for table `tblkeputusan`
--

CREATE TABLE IF NOT EXISTS `tblkeputusan` (
  `idkeputusan` int(30) NOT NULL AUTO_INCREMENT,
  `idalternatif` int(30) NOT NULL,
  `nama_alternatif` varchar(100) NOT NULL,
  `nilai` double NOT NULL,
  PRIMARY KEY (`idkeputusan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tblkeputusan`
--

INSERT INTO `tblkeputusan` (`idkeputusan`, `idalternatif`, `nama_alternatif`, `nilai`) VALUES
(1, 1, 'PT PRO ROLL INTERNATIONAL', 0.334),
(2, 2, 'PT LARIS MANIS', 0.239),
(3, 3, 'PT ANGRAH PHARMINDO LESTARI', 0.184),
(4, 4, 'PT DANLIRIS SUKAHARJO', 0.121),
(5, 5, 'PT DJARUM', 0.08),
(6, 7, 'PT BATAMTEX', 0.043);

-- --------------------------------------------------------

--
-- Table structure for table `tblkriteria`
--

CREATE TABLE IF NOT EXISTS `tblkriteria` (
  `idkriteria` int(30) NOT NULL AUTO_INCREMENT,
  `nama_kriteria` varchar(100) NOT NULL,
  `simbol` varchar(10) NOT NULL,
  PRIMARY KEY (`idkriteria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tblkriteria`
--

INSERT INTO `tblkriteria` (`idkriteria`, `nama_kriteria`, `simbol`) VALUES
(1, 'pengiriman barang', 'A1'),
(2, 'pelayanan', 'A2'),
(3, 'produk', 'A3'),
(4, 'kualitas supplier', 'A4'),
(5, 'biaya', 'A5');

-- --------------------------------------------------------

--
-- Table structure for table `tblnormalisasialternatif`
--

CREATE TABLE IF NOT EXISTS `tblnormalisasialternatif` (
  `idnormalisasi` int(30) NOT NULL AUTO_INCREMENT,
  `idalternatif` int(30) NOT NULL,
  `nama_alias` varchar(20) NOT NULL,
  `PRI` double NOT NULL,
  `LM` double NOT NULL,
  `APL` double NOT NULL,
  `DS` double NOT NULL,
  `JRM` double NOT NULL,
  `BT` double NOT NULL,
  `rata2` double NOT NULL,
  `tanda` int(10) NOT NULL,
  PRIMARY KEY (`idnormalisasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=97 ;

--
-- Dumping data for table `tblnormalisasialternatif`
--

INSERT INTO `tblnormalisasialternatif` (`idnormalisasi`, `idalternatif`, `nama_alias`, `PRI`, `LM`, `APL`, `DS`, `JRM`, `BT`, `rata2`, `tanda`) VALUES
(67, 1, 'PRI', 0.412, 0.568, 0.47, 0.383, 0.108, 0.28, 0.37, 2),
(68, 2, 'LM', 0.137, 0.189, 0.376, 0.191, 0.162, 0.2, 0.209, 2),
(69, 3, 'APL', 0.082, 0.047, 0.094, 0.287, 0.378, 0.24, 0.188, 2),
(70, 4, 'DS', 0.103, 0.095, 0.031, 0.096, 0.27, 0.16, 0.126, 2),
(71, 5, 'JRM', 0.206, 0.063, 0.013, 0.019, 0.054, 0.08, 0.073, 2),
(72, 7, 'BT', 0.059, 0.038, 0.016, 0.024, 0.027, 0.04, 0.034, 2),
(73, 1, 'PRI', 0.233, 0.731, 0.053, 0.243, 0.186, 0.091, 0.256, 3),
(74, 2, 'LM', 0.047, 0.146, 0.743, 0.304, 0.372, 0.136, 0.291, 3),
(75, 3, 'APL', 0.467, 0.021, 0.106, 0.365, 0.124, 0.182, 0.211, 3),
(76, 4, 'DS', 0.058, 0.029, 0.018, 0.061, 0.248, 0.227, 0.107, 3),
(77, 5, 'JRM', 0.078, 0.024, 0.053, 0.015, 0.062, 0.318, 0.092, 3),
(78, 7, 'BT', 0.117, 0.049, 0.027, 0.012, 0.009, 0.045, 0.043, 3),
(79, 1, 'PRI', 0.408, 0.734, 0.365, 0.192, 0.236, 0.136, 0.345, 4),
(80, 2, 'LM', 0.068, 0.122, 0.457, 0.288, 0.331, 0.091, 0.226, 4),
(81, 3, 'APL', 0.102, 0.024, 0.091, 0.384, 0.095, 0.227, 0.154, 4),
(82, 4, 'DS', 0.204, 0.041, 0.023, 0.096, 0.284, 0.182, 0.138, 4),
(83, 5, 'JRM', 0.082, 0.017, 0.046, 0.016, 0.047, 0.318, 0.088, 4),
(84, 7, 'BT', 0.136, 0.061, 0.018, 0.024, 0.007, 0.045, 0.049, 4),
(85, 1, 'PRI', 0.412, 0.782, 0.37, 0.239, 0.247, 0.095, 0.358, 5),
(86, 2, 'LM', 0.059, 0.112, 0.463, 0.479, 0.198, 0.143, 0.242, 5),
(87, 3, 'APL', 0.103, 0.022, 0.093, 0.16, 0.346, 0.286, 0.168, 5),
(88, 4, 'DS', 0.137, 0.019, 0.046, 0.08, 0.148, 0.238, 0.111, 5),
(89, 5, 'JRM', 0.082, 0.028, 0.013, 0.027, 0.049, 0.19, 0.065, 5),
(90, 7, 'BT', 0.206, 0.037, 0.015, 0.016, 0.012, 0.048, 0.056, 5),
(91, 1, 'PRI', 0.408, 0.69, 0.198, 0.28, 0.263, 0.286, 0.354, 1),
(92, 2, 'LM', 0.082, 0.138, 0.595, 0.187, 0.197, 0.19, 0.232, 1),
(93, 3, 'APL', 0.204, 0.023, 0.099, 0.374, 0.132, 0.143, 0.163, 1),
(94, 4, 'DS', 0.136, 0.069, 0.025, 0.093, 0.329, 0.095, 0.125, 1),
(95, 5, 'JRM', 0.102, 0.046, 0.05, 0.019, 0.066, 0.238, 0.087, 1),
(96, 7, 'BT', 0.068, 0.034, 0.033, 0.047, 0.013, 0.048, 0.041, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblnormalisasikriteria`
--

CREATE TABLE IF NOT EXISTS `tblnormalisasikriteria` (
  `idnormalisasi` int(30) NOT NULL AUTO_INCREMENT,
  `idkriteria` int(30) NOT NULL,
  `simbol` varchar(20) NOT NULL,
  `A1` double NOT NULL,
  `A2` double NOT NULL,
  `A3` double NOT NULL,
  `A4` double NOT NULL,
  `A5` double NOT NULL,
  `rata2` double NOT NULL,
  PRIMARY KEY (`idnormalisasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tblnormalisasikriteria`
--

INSERT INTO `tblnormalisasikriteria` (`idnormalisasi`, `idkriteria`, `simbol`, `A1`, `A2`, `A3`, `A4`, `A5`, `rata2`) VALUES
(1, 1, 'A1', 0.438, 0.49, 0.444, 0.324, 0.333, 0.406),
(2, 2, 'A2', 0.219, 0.245, 0.296, 0.243, 0.267, 0.254),
(3, 3, 'A3', 0.146, 0.122, 0.148, 0.324, 0.133, 0.175),
(4, 4, 'A4', 0.11, 0.082, 0.037, 0.081, 0.2, 0.102),
(5, 5, 'A5', 0.088, 0.061, 0.074, 0.027, 0.067, 0.063);

-- --------------------------------------------------------

--
-- Table structure for table `tblrataalternatif`
--

CREATE TABLE IF NOT EXISTS `tblrataalternatif` (
  `idrata` int(30) NOT NULL AUTO_INCREMENT,
  `idalternatif` int(30) NOT NULL,
  `nama_alternatif` varchar(20) NOT NULL,
  `A1` double NOT NULL,
  `A2` double NOT NULL,
  `A3` double NOT NULL,
  `A4` double NOT NULL,
  `A5` double NOT NULL,
  PRIMARY KEY (`idrata`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tblrataalternatif`
--

INSERT INTO `tblrataalternatif` (`idrata`, `idalternatif`, `nama_alternatif`, `A1`, `A2`, `A3`, `A4`, `A5`) VALUES
(1, 1, 'PRI', 0.37, 0.256, 0.345, 0.358, 0.354),
(2, 2, 'LM', 0.209, 0.291, 0.226, 0.242, 0.232),
(3, 3, 'APL', 0.188, 0.211, 0.154, 0.168, 0.163),
(4, 4, 'DS', 0.126, 0.107, 0.138, 0.111, 0.125),
(5, 5, 'JRM', 0.073, 0.092, 0.088, 0.065, 0.087),
(6, 7, 'BT', 0.034, 0.043, 0.049, 0.056, 0.041);

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE IF NOT EXISTS `tbluser` (
  `iduser` int(30) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`iduser`, `username`, `password`) VALUES
(1, 'admin', 'admin2014'),
(2, 'abusairi', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
