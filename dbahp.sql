-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2014 at 04:33 PM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbahp`
--
CREATE DATABASE IF NOT EXISTS `dbahp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dbahp`;

-- --------------------------------------------------------

--
-- Table structure for table `tblalternatif`
--

CREATE TABLE IF NOT EXISTS `tblalternatif` (
  `idalternatif` int(30) NOT NULL AUTO_INCREMENT,
  `nama_alternatif` varchar(100) NOT NULL,
  `nama_alias` varchar(50) NOT NULL,
  PRIMARY KEY (`idalternatif`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tblalternatif`
--

INSERT INTO `tblalternatif` (`idalternatif`, `nama_alternatif`, `nama_alias`) VALUES
(11, 'SYAFI''I MUSLIM', 'SY'),
(12, 'JOKO WIDODO', 'JK'),
(13, 'ABDURRAHMAN', 'ABD'),
(14, 'AHMAD TANTOWI', 'ATW'),
(15, 'ABDUS SYUKUR', 'ABS'),
(16, 'FAWAIIT RIYADI', 'FW'),
(17, 'MISNATI', 'MST');

-- --------------------------------------------------------

--
-- Table structure for table `tblbobotalternatif`
--

CREATE TABLE IF NOT EXISTS `tblbobotalternatif` (
  `idbobot` int(30) NOT NULL AUTO_INCREMENT,
  `idalternatif` int(30) NOT NULL,
  `nama_alias` varchar(20) NOT NULL,
  `SY` double NOT NULL,
  `JK` double NOT NULL,
  `ABD` double NOT NULL,
  `ATW` double NOT NULL,
  `ABS` double NOT NULL,
  `FW` double NOT NULL,
  `MST` double NOT NULL,
  `tanda` int(10) NOT NULL,
  PRIMARY KEY (`idbobot`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `tblbobotalternatif`
--

INSERT INTO `tblbobotalternatif` (`idbobot`, `idalternatif`, `nama_alias`, `SY`, `JK`, `ABD`, `ATW`, `ABS`, `FW`, `MST`, `tanda`) VALUES
(1, 11, 'SY', 1, 4, 7, 6, 0.333, 0.167, 9, 1),
(2, 12, 'JK', 0.25, 1, 8, 0.167, 6, 3, 2, 1),
(3, 13, 'ABD', 0.143, 0.125, 1, 3, 8, 0.5, 0.25, 1),
(4, 14, 'ATW', 0.167, 6, 0.333, 1, 7, 0.25, 6, 1),
(5, 15, 'ABS', 3, 0.167, 0.125, 0.143, 1, 9, 0.143, 1),
(6, 16, 'FW', 6, 0.333, 2, 4, 0.111, 1, 5, 1),
(7, 17, 'MST', 0.111, 0.5, 4, 0.167, 7, 0.2, 1, 1),
(8, 8, 'jumlah', 10.671, 12.125, 22.458, 14.477, 29.444, 14.117, 23.393, 1),
(9, 11, 'SY', 1, 6, 7, 0.25, 0.5, 0.333, 7, 2),
(10, 12, 'JK', 0.167, 1, 4, 5, 9, 5, 2, 2),
(11, 13, 'ABD', 0.143, 0.25, 1, 4, 3, 0.333, 0.167, 2),
(12, 14, 'ATW', 4, 0.2, 0.25, 1, 4, 7, 3, 2),
(13, 15, 'ABS', 2, 0.111, 0.333, 0.25, 1, 4, 7, 2),
(14, 16, 'FW', 3, 0.2, 3, 0.143, 0.25, 1, 0.333, 2),
(15, 17, 'MST', 0.143, 0.5, 6, 0.333, 0.143, 3, 1, 2),
(16, 8, 'jumlah', 10.453, 8.261, 21.583, 10.976, 17.893, 20.666, 20.5, 2),
(17, 11, 'SY', 1, 5, 5, 6, 0.5, 7, 0.333, 3),
(18, 12, 'JK', 0.2, 1, 3, 7, 6, 3, 2, 3),
(19, 13, 'ABD', 0.2, 0.333, 1, 0.143, 0.5, 0.25, 4, 3),
(20, 14, 'ATW', 0.167, 0.143, 7, 1, 3, 3, 5, 3),
(21, 15, 'ABS', 2, 0.167, 2, 0.333, 1, 4, 6, 3),
(22, 16, 'FW', 0.143, 0.333, 0.25, 0.333, 0.25, 1, 4, 3),
(23, 17, 'MST', 3, 0.5, 0.25, 0.2, 0.167, 0.25, 1, 3),
(24, 8, 'jumlah', 6.71, 7.476, 18.5, 15.009, 11.417, 18.5, 22.333, 3),
(25, 11, 'SY', 1, 5, 0.25, 7, 6, 0.25, 0.333, 4),
(26, 12, 'JK', 0.2, 1, 8, 0.25, 8, 0.2, 0.5, 4),
(27, 13, 'ABD', 4, 0.125, 1, 5, 4, 4, 7, 4),
(28, 14, 'ATW', 0.143, 4, 0.2, 1, 0.2, 0.333, 0.25, 4),
(29, 15, 'ABS', 0.167, 0.125, 0.25, 5, 1, 4, 9, 4),
(30, 16, 'FW', 4, 5, 0.25, 3, 0.25, 1, 5, 4),
(31, 17, 'MST', 3, 2, 0.143, 4, 0.111, 0.2, 1, 4),
(32, 8, 'jumlah', 12.51, 17.25, 10.093, 25.25, 19.561, 9.983, 23.083, 4),
(33, 11, 'SY', 1, 4, 0.333, 7, 7, 0.143, 0.5, 5),
(34, 12, 'JK', 0.25, 0.25, 0.333, 0.333, 7, 3, 9, 5),
(35, 13, 'ABD', 3, 3, 1, 4, 9, 0.5, 0.2, 5),
(36, 14, 'ATW', 0.143, 3, 0.25, 1, 6, 0.5, 5, 5),
(37, 15, 'ABS', 0.143, 0.143, 0.111, 0.167, 1, 0.167, 0.5, 5),
(38, 16, 'FW', 7, 0.333, 2, 2, 6, 1, 6, 5),
(39, 17, 'MST', 2, 0.111, 5, 0.2, 2, 0.167, 1, 5),
(40, 8, 'jumlah', 13.536, 10.837, 9.027, 14.7, 38, 5.477, 22.2, 5),
(41, 11, 'SY', 1, 0.333, 7, 9, 7, 0.2, 8, 6),
(42, 12, 'JK', 3, 1, 6, 0.2, 0.333, 5, 4, 6),
(43, 13, 'ABD', 0.143, 0.167, 1, 6, 0.333, 0.5, 0.5, 6),
(44, 14, 'ATW', 0.111, 5, 0.167, 1, 6, 2, 0.5, 6),
(45, 15, 'ABS', 0.143, 3, 3, 0.167, 1, 4, 0.5, 6),
(46, 16, 'FW', 5, 0.2, 2, 0.5, 0.25, 1, 6, 6),
(47, 17, 'MST', 0.125, 0.25, 2, 2, 2, 0.167, 1, 6),
(48, 8, 'jumlah', 9.522, 9.95, 21.167, 18.867, 16.916, 12.867, 20.5, 6),
(49, 11, 'SY', 1, 6, 0.2, 4, 3, 6, 3, 7),
(50, 12, 'JK', 0.167, 1, 5, 6, 0.2, 7, 3, 7),
(51, 13, 'ABD', 5, 0.2, 1, 4, 0.5, 0.2, 0.167, 7),
(52, 14, 'ATW', 0.25, 0.167, 0.25, 1, 8, 4, 0.333, 7),
(53, 15, 'ABS', 0.333, 5, 2, 0.125, 1, 6, 0.5, 7),
(54, 16, 'FW', 0.167, 0.143, 5, 0.25, 0.167, 1, 3, 7),
(55, 17, 'MST', 0.333, 0.333, 6, 3, 2, 0.333, 1, 7),
(56, 8, 'jumlah', 7.25, 12.843, 19.45, 18.375, 14.867, 24.533, 11, 7);

-- --------------------------------------------------------

--
-- Table structure for table `tblbobotkriteria`
--

CREATE TABLE IF NOT EXISTS `tblbobotkriteria` (
  `idbobot` int(30) NOT NULL AUTO_INCREMENT,
  `idkriteria` int(30) NOT NULL,
  `simbol` varchar(20) NOT NULL,
  `A1` double NOT NULL,
  `A2` double NOT NULL,
  `A3` double NOT NULL,
  `A4` double NOT NULL,
  `A5` double NOT NULL,
  `A6` double NOT NULL,
  `A7` double NOT NULL,
  PRIMARY KEY (`idbobot`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tblbobotkriteria`
--

INSERT INTO `tblbobotkriteria` (`idbobot`, `idkriteria`, `simbol`, `A1`, `A2`, `A3`, `A4`, `A5`, `A6`, `A7`) VALUES
(1, 1, 'A1', 1, 4, 0.333, 4, 7, 8, 9),
(2, 2, 'A2', 0.25, 1, 6, 0.143, 0.125, 5, 2),
(3, 3, 'A3', 3, 0.167, 1, 0.25, 0.143, 0.5, 2),
(4, 4, 'A4', 0.25, 7, 4, 1, 0.25, 7, 8),
(5, 5, 'A5', 0.143, 8, 7, 4, 1, 0.143, 5),
(6, 10, 'A6', 0.125, 0.2, 2, 0.143, 7, 1, 0.333),
(7, 11, 'A7', 0.111, 0.5, 0.5, 0.125, 0.2, 3, 1),
(8, 8, 'jumlah', 4.879, 20.867, 20.833, 9.661, 15.718, 24.643, 27.333);

-- --------------------------------------------------------

--
-- Table structure for table `tblkeputusan`
--

CREATE TABLE IF NOT EXISTS `tblkeputusan` (
  `idkeputusan` int(30) NOT NULL AUTO_INCREMENT,
  `idalternatif` int(30) NOT NULL,
  `nama_alternatif` varchar(100) NOT NULL,
  `nilai` double NOT NULL,
  PRIMARY KEY (`idkeputusan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tblkeputusan`
--

INSERT INTO `tblkeputusan` (`idkeputusan`, `idalternatif`, `nama_alternatif`, `nilai`) VALUES
(1, 12, 'JOKO WIDODO', 0.182),
(2, 13, 'ABDURRAHMAN', 0.124),
(3, 14, 'AHMAD TANTOWI', 0.132),
(4, 15, 'ABDUS SYUKUR', 0.116),
(5, 16, 'FAWAIIT RIYADI', 0.152),
(6, 17, 'MISNATI', 0.089);

-- --------------------------------------------------------

--
-- Table structure for table `tblkriteria`
--

CREATE TABLE IF NOT EXISTS `tblkriteria` (
  `idkriteria` int(30) NOT NULL AUTO_INCREMENT,
  `nama_kriteria` varchar(100) NOT NULL,
  `simbol` varchar(10) NOT NULL,
  PRIMARY KEY (`idkriteria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tblkriteria`
--

INSERT INTO `tblkriteria` (`idkriteria`, `nama_kriteria`, `simbol`) VALUES
(1, 'Kepemilikan Rumah', 'A1'),
(2, 'Memiliki Aset Pribadi', 'A2'),
(3, 'Penghasilan', 'A3'),
(4, 'Jumlah Tanggungan', 'A4'),
(5, 'Kemampuan Membeli Daging dalam Seminggu', 'A5'),
(10, 'Kemampuan Berobat Kerumah Sakit', 'A6'),
(11, 'Memiliki Tanah', 'A7');

-- --------------------------------------------------------

--
-- Table structure for table `tblnormalisasialternatif`
--

CREATE TABLE IF NOT EXISTS `tblnormalisasialternatif` (
  `idnormalisasi` int(30) NOT NULL AUTO_INCREMENT,
  `idalternatif` int(30) NOT NULL,
  `nama_alias` varchar(20) NOT NULL,
  `SY` double NOT NULL,
  `JK` double NOT NULL,
  `ABD` double NOT NULL,
  `ATW` double NOT NULL,
  `ABS` double NOT NULL,
  `FW` double NOT NULL,
  `MST` double NOT NULL,
  `rata2` double NOT NULL,
  `tanda` int(10) NOT NULL,
  PRIMARY KEY (`idnormalisasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `tblnormalisasialternatif`
--

INSERT INTO `tblnormalisasialternatif` (`idnormalisasi`, `idalternatif`, `nama_alias`, `SY`, `JK`, `ABD`, `ATW`, `ABS`, `FW`, `MST`, `rata2`, `tanda`) VALUES
(1, 11, 'SY', 0.094, 0.33, 0.312, 0.414, 0.011, 0.012, 0.385, 0.223, 1),
(2, 12, 'JK', 0.023, 0.082, 0.356, 0.012, 0.204, 0.213, 0.085, 0.139, 1),
(3, 13, 'ABD', 0.013, 0.01, 0.045, 0.207, 0.272, 0.035, 0.011, 0.085, 1),
(4, 14, 'ATW', 0.016, 0.495, 0.015, 0.069, 0.238, 0.018, 0.256, 0.158, 1),
(5, 15, 'ABS', 0.281, 0.014, 0.006, 0.01, 0.034, 0.638, 0.006, 0.141, 1),
(6, 16, 'FW', 0.562, 0.027, 0.089, 0.276, 0.004, 0.071, 0.214, 0.178, 1),
(7, 17, 'MST', 0.01, 0.041, 0.178, 0.012, 0.238, 0.014, 0.043, 0.077, 1),
(8, 11, 'SY', 0.096, 0.726, 0.324, 0.023, 0.028, 0.016, 0.341, 0.222, 2),
(9, 12, 'JK', 0.016, 0.121, 0.185, 0.456, 0.503, 0.242, 0.098, 0.232, 2),
(10, 13, 'ABD', 0.014, 0.03, 0.046, 0.364, 0.168, 0.016, 0.008, 0.092, 2),
(11, 14, 'ATW', 0.383, 0.024, 0.012, 0.091, 0.224, 0.339, 0.146, 0.174, 2),
(12, 15, 'ABS', 0.191, 0.013, 0.015, 0.023, 0.056, 0.194, 0.341, 0.119, 2),
(13, 16, 'FW', 0.287, 0.024, 0.139, 0.013, 0.014, 0.048, 0.016, 0.077, 2),
(14, 17, 'MST', 0.014, 0.061, 0.278, 0.03, 0.008, 0.145, 0.049, 0.084, 2),
(15, 11, 'SY', 0.149, 0.669, 0.27, 0.4, 0.044, 0.378, 0.015, 0.275, 3),
(16, 12, 'JK', 0.03, 0.134, 0.162, 0.466, 0.526, 0.162, 0.09, 0.224, 3),
(17, 13, 'ABD', 0.03, 0.045, 0.054, 0.01, 0.044, 0.014, 0.179, 0.054, 3),
(18, 14, 'ATW', 0.025, 0.019, 0.378, 0.067, 0.263, 0.162, 0.224, 0.163, 3),
(19, 15, 'ABS', 0.298, 0.022, 0.108, 0.022, 0.088, 0.216, 0.269, 0.146, 3),
(20, 16, 'FW', 0.021, 0.045, 0.014, 0.022, 0.022, 0.054, 0.179, 0.051, 3),
(21, 17, 'MST', 0.447, 0.067, 0.014, 0.013, 0.015, 0.014, 0.045, 0.088, 3),
(22, 11, 'SY', 0.08, 0.29, 0.025, 0.277, 0.307, 0.025, 0.014, 0.145, 4),
(23, 12, 'JK', 0.016, 0.058, 0.793, 0.01, 0.409, 0.02, 0.022, 0.19, 4),
(24, 13, 'ABD', 0.32, 0.007, 0.099, 0.198, 0.204, 0.401, 0.303, 0.219, 4),
(25, 14, 'ATW', 0.011, 0.232, 0.02, 0.04, 0.01, 0.033, 0.011, 0.051, 4),
(26, 15, 'ABS', 0.013, 0.007, 0.025, 0.198, 0.051, 0.401, 0.39, 0.155, 4),
(27, 16, 'FW', 0.32, 0.29, 0.025, 0.119, 0.013, 0.1, 0.217, 0.155, 4),
(28, 17, 'MST', 0.24, 0.116, 0.014, 0.158, 0.006, 0.02, 0.043, 0.085, 4),
(29, 11, 'SY', 0.074, 0.369, 0.037, 0.476, 0.184, 0.026, 0.023, 0.17, 5),
(30, 12, 'JK', 0.018, 0.023, 0.037, 0.023, 0.184, 0.548, 0.405, 0.177, 5),
(31, 13, 'ABD', 0.222, 0.277, 0.111, 0.272, 0.237, 0.091, 0.009, 0.174, 5),
(32, 14, 'ATW', 0.011, 0.277, 0.028, 0.068, 0.158, 0.091, 0.225, 0.123, 5),
(33, 15, 'ABS', 0.011, 0.013, 0.012, 0.011, 0.026, 0.03, 0.023, 0.018, 5),
(34, 16, 'FW', 0.517, 0.031, 0.222, 0.136, 0.158, 0.183, 0.27, 0.217, 5),
(35, 17, 'MST', 0.148, 0.01, 0.554, 0.014, 0.053, 0.03, 0.045, 0.122, 5),
(36, 11, 'SY', 0.105, 0.033, 0.331, 0.477, 0.414, 0.016, 0.39, 0.252, 6),
(37, 12, 'JK', 0.315, 0.101, 0.283, 0.011, 0.02, 0.389, 0.195, 0.188, 6),
(38, 13, 'ABD', 0.015, 0.017, 0.047, 0.318, 0.02, 0.039, 0.024, 0.069, 6),
(39, 14, 'ATW', 0.012, 0.503, 0.008, 0.053, 0.355, 0.155, 0.024, 0.159, 6),
(40, 15, 'ABS', 0.015, 0.302, 0.142, 0.009, 0.059, 0.311, 0.024, 0.123, 6),
(41, 16, 'FW', 0.525, 0.02, 0.094, 0.027, 0.015, 0.078, 0.293, 0.15, 6),
(42, 17, 'MST', 0.013, 0.025, 0.094, 0.106, 0.118, 0.013, 0.049, 0.06, 6),
(43, 11, 'SY', 0.138, 0.467, 0.01, 0.218, 0.202, 0.245, 0.273, 0.222, 7),
(44, 12, 'JK', 0.023, 0.078, 0.257, 0.327, 0.013, 0.285, 0.273, 0.179, 7),
(45, 13, 'ABD', 0.69, 0.016, 0.051, 0.218, 0.034, 0.008, 0.015, 0.147, 7),
(46, 14, 'ATW', 0.034, 0.013, 0.013, 0.054, 0.538, 0.163, 0.03, 0.121, 7),
(47, 15, 'ABS', 0.046, 0.389, 0.103, 0.007, 0.067, 0.245, 0.045, 0.129, 7),
(48, 16, 'FW', 0.023, 0.011, 0.257, 0.014, 0.011, 0.041, 0.273, 0.09, 7),
(49, 17, 'MST', 0.046, 0.026, 0.308, 0.163, 0.135, 0.014, 0.091, 0.112, 7);

-- --------------------------------------------------------

--
-- Table structure for table `tblnormalisasikriteria`
--

CREATE TABLE IF NOT EXISTS `tblnormalisasikriteria` (
  `idnormalisasi` int(30) NOT NULL AUTO_INCREMENT,
  `idkriteria` int(30) NOT NULL,
  `simbol` varchar(20) NOT NULL,
  `A1` double NOT NULL,
  `A2` double NOT NULL,
  `A3` double NOT NULL,
  `A4` double NOT NULL,
  `A5` double NOT NULL,
  `A6` double NOT NULL,
  `A7` double NOT NULL,
  `rata2` double NOT NULL,
  PRIMARY KEY (`idnormalisasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tblnormalisasikriteria`
--

INSERT INTO `tblnormalisasikriteria` (`idnormalisasi`, `idkriteria`, `simbol`, `A1`, `A2`, `A3`, `A4`, `A5`, `A6`, `A7`, `rata2`) VALUES
(1, 1, 'A1', 0.205, 0.192, 0.016, 0.414, 0.445, 0.325, 0.329, 0.275),
(2, 2, 'A2', 0.051, 0.048, 0.288, 0.015, 0.008, 0.203, 0.073, 0.098),
(3, 3, 'A3', 0.615, 0.008, 0.048, 0.026, 0.009, 0.02, 0.073, 0.114),
(4, 4, 'A4', 0.051, 0.335, 0.192, 0.104, 0.016, 0.284, 0.293, 0.182),
(5, 5, 'A5', 0.029, 0.383, 0.336, 0.414, 0.064, 0.006, 0.183, 0.202),
(6, 10, 'A6', 0.026, 0.01, 0.096, 0.015, 0.445, 0.041, 0.012, 0.092),
(7, 11, 'A7', 0.023, 0.024, 0.024, 0.013, 0.013, 0.122, 0.037, 0.037);

-- --------------------------------------------------------

--
-- Table structure for table `tblrataalternatif`
--

CREATE TABLE IF NOT EXISTS `tblrataalternatif` (
  `idrata` int(30) NOT NULL AUTO_INCREMENT,
  `idalternatif` int(30) NOT NULL,
  `nama_alternatif` varchar(100) NOT NULL,
  `A1` double NOT NULL,
  `A2` double NOT NULL,
  `A3` double NOT NULL,
  `A4` double NOT NULL,
  `A5` double NOT NULL,
  `A6` double NOT NULL,
  `A7` double NOT NULL,
  PRIMARY KEY (`idrata`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tblrataalternatif`
--

INSERT INTO `tblrataalternatif` (`idrata`, `idalternatif`, `nama_alternatif`, `A1`, `A2`, `A3`, `A4`, `A5`, `A6`, `A7`) VALUES
(1, 11, 'SY', 0.223, 0.222, 0.275, 0.145, 0.17, 0.252, 0.222),
(2, 12, 'JK', 0.139, 0.232, 0.224, 0.19, 0.177, 0.188, 0.179),
(3, 13, 'ABD', 0.085, 0.092, 0.054, 0.219, 0.174, 0.069, 0.147),
(4, 14, 'ATW', 0.158, 0.174, 0.163, 0.051, 0.123, 0.159, 0.121),
(5, 15, 'ABS', 0.141, 0.119, 0.146, 0.155, 0.018, 0.123, 0.129),
(6, 16, 'FW', 0.178, 0.077, 0.051, 0.155, 0.217, 0.15, 0.09),
(7, 17, 'MST', 0.077, 0.084, 0.088, 0.085, 0.122, 0.06, 0.112);

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE IF NOT EXISTS `tbluser` (
  `iduser` int(30) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`iduser`, `username`, `password`) VALUES
(1, 'admin', 'admin2014'),
(2, 'triash', 'password');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
