<h4 class="widgettitle">Data Kriteria</h4>
<table class="table table-bordered">
    <thead>
        <tr>
            <th class="head0">Nama Kriteria</th>
			<th class="head0">Nama Alias</th>
			<th class="head0">Bobot</th>
        </tr>
    </thead>
	<tbody>
		<?php
			$sql = mysql_query("SELECT * FROM tblprefrensi ORDER BY idprefrensi ASC");
			while($data = mysql_fetch_array($sql))
			{
		?>
        <tr class="gradeX">
            <td><?php echo $data['nama_kriteria']?></td>
			<td><?php echo $data['alias']?></td>
			<td><?php echo $data['bobot']?></td>
        </tr>
		<?php
			}
		?>
    </tbody>
</table>
<br/>
<h4 class="widgettitle">Data Sub Kriteria</h4>
<table class="table table-bordered">
    <thead>
        <tr>
            <th class="head0">Nama Kriteria</th>
			<th class="head0">Nama Alias</th>
			<th class="head0">Nilai Kriteria</th>
			<th class="head0">Bobot</th>
        </tr>
    </thead>
	<tbody>
		<?php
			$sql = mysql_query("SELECT * FROM tblkriteria ORDER BY idkriteria ASC");
			while($data = mysql_fetch_array($sql))
			{
		?>
        <tr class="gradeX">
            <td><?php echo $data['nama_kriteria']?></td>
			<td><?php echo $data['alias']?></td>
			<td><?php echo $data['rentang']?></td>
			<td><?php echo $data['bobot']?></td>
        </tr>
		<?php
			}
		?>
    </tbody>
</table>
<br/>
<h4 class="widgettitle">Data Pengujian</h4>
<table class="table table-bordered">
    <thead>
        <tr>
            <th class="head0">Nama Peternak</th>
            <?php
				$sql = mysql_query("SELECT DISTINCT(alias) FROM tblkriteria ORDER BY idkriteria ASC");
				while($data = mysql_fetch_array($sql))
				{
			?>
			<th class="head0" align="center"><?php echo $data['alias']?></th>
			<?php
				}
			?>
        </tr>
    </thead>
	<tbody>
		<?php
			$sql = mysql_query("SELECT * FROM tblalternatif ORDER BY idalternatif ASC");
			while($data = mysql_fetch_array($sql))
			{
		?>
        <tr class="gradeX">
            <td><?php echo $data['nama_peternak']?></td>
			<?php
				$sql_alias = mysql_query("SELECT DISTINCT(alias) FROM tblkriteria ORDER BY idkriteria ASC");
				while($data_alias = mysql_fetch_array($sql_alias))
				{
					$alias = $data_alias['alias'];
			?>
			<td><?php echo $data[$alias]?></td>
			<?php
				}
			?>
        </tr>
		<?php
			}
		?>
    </tbody>
</table>
<br/>
<!--Data pembobotan-->
<h4 class="widgettitle">Hasil Pembobotan Data Pengujian</h4>
<table class="table table-bordered">
    <thead>
        <tr>
            <th class="head0">Nama Peternak</th>
            <?php
				$sql = mysql_query("SELECT DISTINCT(alias) FROM tblkriteria ORDER BY idkriteria ASC");
				while($data = mysql_fetch_array($sql))
				{
			?>
			<th class="head0" align="center"><?php echo $data['alias']?></th>
			<?php
				}
			?>
        </tr>
    </thead>
	<tbody>
		<?php
			$sql = mysql_query("SELECT * FROM tblbobot ORDER BY idalternatif ASC");
			while($data = mysql_fetch_array($sql))
			{
		?>
        <tr class="gradeX">
            <td><?php echo $data['nama_peternak']?></td>
			<?php
				$sql_alias = mysql_query("SELECT DISTINCT(alias) FROM tblkriteria ORDER BY idkriteria ASC");
				while($data_alias = mysql_fetch_array($sql_alias))
				{
					$alias = $data_alias['alias'];
			?>
			<td><?php echo $data[$alias]?></td>
			<?php
				}
			?>
        </tr>
		<?php
			}
		?>
    </tbody>
</table>
<br/>
<h4 class="widgettitle">Hasil Normalisasi Bobot Data Pengujian</h4>
<table class="table table-bordered">
    <thead>
        <tr>
            <th class="head0">Nama Peternak</th>
            <?php
				$sql = mysql_query("SELECT DISTINCT(alias) FROM tblkriteria ORDER BY idkriteria ASC");
				while($data = mysql_fetch_array($sql))
				{
			?>
			<th class="head0" align="center"><?php echo $data['alias']?></th>
			<?php
				}
			?>
        </tr>
    </thead>
	<tbody>
		<?php
			$sql = mysql_query("SELECT * FROM tblnormalisasi ORDER BY idalternatif ASC");
			while($data = mysql_fetch_array($sql))
			{
		?>
        <tr class="gradeX">
            <td><?php echo $data['nama_peternak']?></td>
			<?php
				$sql_alias = mysql_query("SELECT DISTINCT(alias) FROM tblkriteria ORDER BY idkriteria ASC");
				while($data_alias = mysql_fetch_array($sql_alias))
				{
					$alias = $data_alias['alias'];
			?>
			<td><?php echo $data[$alias]?></td>
			<?php
				}
			?>
        </tr>
		<?php
			}
		?>
    </tbody>
</table>
<br/>
<h4 class="widgettitle">Hasil Perkalian Bobot Prefrensi dan Nilai Normalisasi</h4>
<table class="table table-bordered">
    <thead>
        <tr>
            <th class="head0">Nama Peternak</th>
            <?php
				$sql = mysql_query("SELECT DISTINCT(alias) FROM tblkriteria ORDER BY idkriteria ASC");
				while($data = mysql_fetch_array($sql))
				{
			?>
			<th class="head0" align="center"><?php echo $data['alias']?></th>
			<?php
				}
			?>
        </tr>
    </thead>
	<tbody>
		<?php
			$sql = mysql_query("SELECT * FROM tblhasilkali ORDER BY idalternatif ASC");
			while($data = mysql_fetch_array($sql))
			{
		?>
        <tr class="gradeX">
            <td><?php echo $data['nama_peternak']?></td>
			<?php
				$sql_alias = mysql_query("SELECT DISTINCT(alias) FROM tblkriteria ORDER BY idkriteria ASC");
				while($data_alias = mysql_fetch_array($sql_alias))
				{
					$alias = $data_alias['alias'];
			?>
			<td><?php echo $data[$alias]?></td>
			<?php
				}
			?>
        </tr>
		<?php
			}
		?>
    </tbody>
</table>
<br/>
<h4 class="widgettitle">Nilai Solusi Edial Positif dan Negarif</h4>
<table class="table table-bordered">
    <thead>
        <tr>
            <th class="head0">Solusi Edial</th>
			<?php
				$sql = mysql_query("SELECT DISTINCT(alias) FROM tblkriteria ORDER BY idkriteria ASC");
				while($data = mysql_fetch_array($sql))
				{
			?>
			<th class="head0" align="center"><?php echo $data['alias']?></th>
			<?php
				}
			?>
        </tr>
    </thead>
	<tbody>
		<?php
			$sql = mysql_query("SELECT * FROM tblsolusiedial ORDER BY idsolusi ASC");
			while($data = mysql_fetch_array($sql))
			{
		?>
        <tr class="gradeX">
            <td><?php echo $data['edial']?></td>
			<?php
				$sql_alias = mysql_query("SELECT DISTINCT(alias) FROM tblkriteria ORDER BY idkriteria ASC");
				while($data_alias = mysql_fetch_array($sql_alias))
				{
					$alias = $data_alias['alias'];
			?>
			<td><?php echo $data[$alias]?></td>
			<?php
				}
			?>
        </tr>
		<?php
			}
		?>
    </tbody>
</table>
<br/>
<h4 class="widgettitle">Nilai Jarak Solusi Edial Positif dan Negatif</h4>
<table class="table table-bordered">
    <thead>
        <tr>
            <th class="head0">Nama Peternak</th>
			<th class="head0">Jarak Solusi Edial Positif (D+)</th>
			<th class="head0">Jarak Solusi Edial Negatif (D-)</th>
        </tr>
    </thead>
	<tbody>
		<?php
			$sql = mysql_query("SELECT * FROM tbljarak ORDER BY idjarak ASC");
			while($data = mysql_fetch_array($sql))
			{
		?>
        <tr class="gradeX">
            <td><?php echo $data['nama_peternak']?></td>
			<td><?php echo $data['dpositif']?></td>
			<td><?php echo $data['dnegatif']?></td>
		</tr>
		<?php
			}
		?>
    </tbody>
</table>
<br/>
<h4 class="widgettitle">Keputusan Akhir</h4>
<table class="table table-bordered">
    <thead>
        <tr>
			<th class="head0">No</th>
            <th class="head0">Nama Peternak</th>
			<th class="head0">Nilai</th>
        </tr>
    </thead>
	<tbody>
		<?php
			$sql = mysql_query("SELECT * FROM tblkeputusan ORDER BY NA DESC");
			$no=0;
			while($data = mysql_fetch_array($sql))
			{
				$no++;
		?>
        <tr class="gradeX">
			<td align="center"><?php echo $no?></td>
            <td><?php echo $data['nama_peternak']?></td>
			<td><?php echo $data['NA']?></td>
		</tr>
		<?php
			}
		?>
    </tbody>
</table>
<p style="color:red;font-size:larger;font-size:20px">Keterangan : Semakin besar nilai yang diperoleh, maka obat akan semakin dipreoritaskan</p>
<button type="submit" class="btn btn-primary" onclick="window.open('cetak-keputusan.php','new','width=1100,height=650,menubars=no');"><span class="icon-print"></span>&nbsp;Cetak Hasil Keputusan</button>
<br><br>