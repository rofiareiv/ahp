<?php
	include"../db/koneksi.php";
	$tombol = $_POST['btnproses'];
	//========================proses untuk kriteria========================
	if($tombol=="simpan_kriteria")
	{
		$nama_kriteria = $_POST['kriteria'];
		$bobot = $_POST['bobot'];
		//cek duplikasi
		$cek = mysql_query("SELECT * FROM tblprefrensi WHERE nama_kriteria='$nama_kriteria'");
		if($ada = mysql_fetch_array($cek))
		{
			echo"<script>alert('Nama Kriteria Duplikat,Silahkan Ganti...!');location.href=\"index-admin.php?action=kriteria\";</script>";
		}
		else
		{
			//cek jumlah kriteria
			$sql_jumlah = mysql_query("SELECT COUNT(idprefrensi) as jumlah FROM tblprefrensi");
			$data_jumlah = mysql_fetch_array($sql_jumlah);
			$jumlah = $data_jumlah['jumlah'];
			$jumlah = $jumlah+1;
			//cek duplikasi
			$cek = mysql_query("SELECT 	nama_kriteria FROM tblprefrensi WHERE nama_kriteria='$nama_kriteria'");
			if(!$ada = mysql_fetch_array($cek))
			{
				//simpan data kriteria
				$simpan_kriteria = mysql_query("INSERT INTO tblprefrensi VALUES(NULL,'$nama_kriteria','K$jumlah','$bobot')");
				echo"<script>alert('Kriteria Baru Sudah Tersimpan....!');location.href=\"index-admin.php?action=kriteria\";</script>";
			}
			else
				echo"<script>alert('Kriteria $nama_kriteria Duplikat, Input yang Lain....!');location.href=\"index-admin.php?action=kriteria\";</script>";
		}
	}
	else if($tombol=="kosongkan_kriteria")
	{
		$kosong = mysql_query("TRUNCATE TABLE tblprefrensi");
		$hapus = mysql_query("DROP TABLE tblalternatif");
		$hapus = mysql_query("DROP TABLE tblbobot");
		echo"<script>alert('Data Kriteria Sudah Dikosongi..!');location.href=\"index-admin.php?action=kriteria\";</script>";
	}
	//==========================akhir proses untuk kriteria==========================
	//==========================awal proses untuk sub kriteria=======================
	else if($tombol=="simpan_subkriteria")
	{
		$idkriteria = $_POST['nama_kriteria'];
		$rentang = $_POST['semesta'];
		$bobot = $_POST['bobot'];
		//ambil nama kriteria dan alis
		$sql = mysql_query("SELECT nama_kriteria,alias FROM tblprefrensi WHERE idprefrensi='$idkriteria'");
		$data = mysql_fetch_array($sql);
		$nama_kriteria = $data['nama_kriteria'];
		$alias = $data['alias'];
		//cek duplikasi
		$cek = mysql_query("SELECT * FROM tblkriteria WHERE nama_kriteria='$nama_kriteria' AND rentang='$rentang'");
		if($ada = mysql_fetch_array($cek))
		{
			echo"<script>alert('Nama Sub Kriteria Duplikat,Silahkan Ganti...!');location.href=\"index-admin.php?action=kriteria\";</script>";
		}
		else
		{
			//simpan data sub kriteria
			$simpan_kriteria = mysql_query("INSERT INTO tblkriteria VALUES(NULL,'$nama_kriteria','$rentang','$bobot','$alias')");
			echo"<script>alert('Sub Kriteria Baru Sudah Tersimpan....!');location.href=\"index-admin.php?action=kriteria\";</script>";
		}
	}
	else if($tombol=="kosongkan_subkriteria")
	{
		$kosong = mysql_query("TRUNCATE TABLE tblkriteria");
		echo"<script>alert('Data Sub Kriteria Sudah Dikosongi..!');location.href=\"index-admin.php?action=kriteria\";</script>";
	}
	//=================================akhir proses sub kriteria====================================
	//=============hama=======================
	else if($tombol=="simpan_hama")
	{
		$nama_hama = strtoupper($_POST['nama_hama']);
		$cek = mysql_query("SELECT * FROM tblhama WHERE nama_hama='$nama_hama'");
		if($ada = mysql_fetch_array($cek))
		{
			echo"<script>alert('Nama Hama Duplikat,Silahkan Ganti...!');location.href=\"index-admin.php?action=hama\";</script>";
		}
		else
		{
			$simpan_hama = mysql_query("INSERT INTO tblhama VALUES(NULL,'$nama_hama')");
			echo"<script>alert('Data Hama Baru Sudah Disimpan..!');location.href=\"index-admin.php?action=hama\";</script>";
		}
	}
	else if($tombol=="simpan_perubahan_hama")
	{
		foreach($_POST['id_hama'] as $index => $id_hama)
		{
			$nama_hama = strtoupper($_POST['nama_hama'][$index]);
			//update data hama
			$update_penduduk = mysql_query("UPDATE tblhama SET nama_hama='$nama_hama' WHERE id_hama='$id_hama'");
		}
		echo"<script>alert('Perubahan Data Hama Sudah Disimpan..!');location.href=\"index-admin.php?action=hama\";</script>";
	}
	else if($tombol=="hapus_hama")
	{
		if(empty($_POST['id_hama_hapus']))
			echo"<script>alert('Anda Belum Memilih Data Hama yang Akan Dihapus, Silahkan Dipilih Terlebih Dahulu...!');location.href=\"index-admin.php?action=hama\";</script>";
		else
		{
			foreach($_POST['id_hama_hapus'] as $index => $id_hama)
			{
				//delete data hama
				$hapus_data = mysql_query("DELETE FROM tblhama WHERE id_hama='$id_hama'");
			}
			echo"<script>alert('Data Hama yang Dipilih Berhasil Dihapus...!');location.href=\"index-admin.php?action=hama\";</script>";
		}
	}
	//=================================awal proses untuk alternatif=================================
	else if($tombol=="simpan_alternatif")
	{
		$nama_peternak = addslashes($_POST['nama_peternak']);
		//echo $nama_peternak." ";
		$sql_jumlah = mysql_query("SELECT COUNT(alias) AS jumlah FROM tblprefrensi");
		if($jumlahnya = mysql_fetch_array($sql_jumlah))
		{
			$jumlah = $jumlahnya['jumlah'];
		}
		$sql = mysql_query("SELECT alias FROM tblprefrensi ORDER BY alias ASC");
		$ke=0;
		$tambahan="";
		while($data = mysql_fetch_array($sql))
		{
			$ke++;
			$alias = $data['alias'];
			$nilai_a = $_POST[$alias];
			//konversi nilai alternatif ke rangking bobot
			if($ke<$jumlah)
				$tambahan.="'".$nilai_a."',";
			else
				$tambahan.="'".$nilai_a."'";
		}
		//simpan data alternatif
		$simpan = mysql_query("INSERT INTO tblalternatif VALUES(NULL,'$nama_peternak',$tambahan)");
		//
		echo"<script>alert('Data Alternatif Sudah Tersimpan....!');location.href=\"index-admin.php?action=alternatif\";</script>";
	}
	else if($tombol=="hapus_alternatif")
	{
		if(empty($_POST['id_a']))
		{
			echo"<script>alert('Tida Ada Data Alternatif yang Dipilih Untuk Dihapus....!');location.href=\"index-admin.php?action=alternatif\";</script>";
		}
		else
		{
			$id_a=$_POST['id_a'];
			foreach($id_a as $idnya)
			{
				$sql=mysql_query("DELETE FROM tblalternatif WHERE idalternatif='$idnya'");
			}
			echo"<script>alert('Data Alternatif yang Dipilih Sudah Dihapus..!');location.href=\"index-admin.php?action=alternatif\";</script>";
		}
	}
	else if($tombol=="kosongkan_alternatif")
	{
		$sql=mysql_query("TRUNCATE TABLE tblalternatif");
		$sql=mysql_query("TRUNCATE TABLE tblbobot");
		echo"<script>alert('Semua Data Alternatif Sudah Dihapus..!');location.href=\"index-admin.php?action=alternatif\";</script>";
	}
	else if($tombol=="buat_bobot")
	{
		//kosongkan dulu
		$kosong = mysql_query("TRUNCATE TABLE tblbobot");
		//ambil data alternatif
		$b_jalan="";
		$b_tkandang="";
		$b_kejujuran="";
		$b_telaten="";
		$b_jarak="";
		$b_keamanan="";
		//$b_jumkandang="";
		$b_kapasitas="";
		$b_air="";
		$sql = mysql_query("SELECT * FROM tblalternatif ORDER BY idalternatif ASC");
		while($data = mysql_fetch_array($sql))
		{
			$idalternatif = $data['idalternatif'];
			$nama_peternak = addslashes($data['nama_peternak']);
			//echo $idalternatif." ".$nama_peternak." ";
			//ambil alias
			$sql_alias = mysql_query("SELECT alias,nama_kriteria FROM tblprefrensi ORDER BY alias ASC");
			while($data_alias = mysql_fetch_array($sql_alias))
			{
				$alias = $data_alias['alias'];
				$nama_kriteria = $data_alias['nama_kriteria'];
				//ambil data alternatif setiap alias
				$nilai = $data[$alias];
				//konfersi nilai alternatif
				if($nama_kriteria=="Akses Jalan")
				{
					if($nilai=="Baik")
						$b_jalan=3;
					else if($nilai=="Cukup")
						$b_jalan=2;
					else if($nilai=="Kurang")
						$b_jalan=1;
				}
				else if($nama_kriteria=="Tinggi Kandang")
				{
					if($nilai>=1 && $nilai<=2)
						$b_tkandang=1;
					else if($nilai>=2 && $nilai<=3)
						$b_tkandang=2;
					else if($nilai>3)
						$b_tkandang=3;
				}
				else if($nama_kriteria=="Kejujuran")
				{
					if($nilai=="Jujur")
						$b_kejujuran=3;
					else if($nilai=="Cukup Jujur")
						$b_kejujuran=2;
					else if($nilai=="Kurang Jujur")
						$b_kejujuran=1;
				}
				else if($nama_kriteria=="Ketelatenan")
				{
					if($nilai=="Telaten")
						$b_telaten=3;
					else if($nilai=="Cukup Telaten")
						$b_telaten=2;
					else if($nilai=="Kurang Telaten")
						$b_telaten=1;
				}
				else if($nama_kriteria=="Jarak Antar Kandang")
				{
					if($nilai>=0 && $nilai<=20)
						$b_jarak=1;
					else if($nilai>20 && $nilai<=40)
						$b_jarak=2;
					else if($nilai>40 && $nilai<=60)
						$b_jarak=3;
					else if($nilai>60 && $nilai<=80)
						$b_jarak=4;
					else if($nilai>80)
						$b_jarak=5;
				}
				else if($nama_kriteria=="Keamanan")
				{
					if($nilai=="Aman")
						$b_keamanan=3;
					else if($nilai=="Cukup Aman")
						$b_keamanan=2;
					else if($nilai=="Kurang Aman")
						$b_keamanan=1;
				}
				/*else if($nama_kriteria=="Jumlah Kandang Disekitar")
				{
					if($nilai>=0 && $nilai<=2)
						$b_jumkandang=4;
					else if($nilai>2 && $nilai<=5)
						$b_jumkandang=3;
					else if($nilai>5 && $nilai<=7)
						$b_jumkandang=2;
					else if($nilai>7)
						$b_jumkandang=1;
				}*/
				else if($nama_kriteria=="Kapasitas Daya Tampung")
				{
					if($nilai>=0 && $nilai<=500)
						$b_kapasitas=1;
					else if($nilai>500 && $nilai<=1000)
						$b_kapasitas=2;
					else if($nilai>1000 && $nilai<=2000)
						$b_kapasitas=3;
					else if($nilai>2000)
						$b_kapasitas=4;
				}
				else if($nama_kriteria=="Ketersediaan Air")
				{
					if($nilai=="Banyak")
						$b_air=3;
					else if($nilai=="Cukup")
						$b_air=2;
					else if($nilai=="Kurang")
						$b_air=1;
				}
			}
			//echo $b_jalan." ".$b_tkandang." ".$b_kejujuran." ".$b_telaten." ".$b_jarak." ".$b_keamanan." ".$b_jumkandang." ".$b_kapasitas." ".$b_air."<br>";
			$simpan = mysql_query("INSERT INTO tblbobot VALUES(NULL,'$idalternatif','$nama_peternak','$b_jalan','$b_tkandang','$b_kejujuran','$b_telaten','$b_air','$b_jarak','$b_keamanan','$b_kapasitas')");
		}
		echo"<script>alert('Pemberian Bobot Pada Data Alternatif Selesai....!');location.href=\"index-admin.php?action=alternatif\";</script>";
	}
	else if($tombol=="simpan_user")
	{
		// setting nama folder tempat upload
		$namafolder = 'foto/';
		if(empty($_FILES['foto']['name']))
		{
			//echo "Nama File : ".$fileName;
			echo"<script>alert('Tidak Ada File yang Diupload');location.href=\"index-admin.php?action=user\";</script>";
		}
		else
		{
			// name file
			$fileName   = $_FILES['foto']['name'];
			// nama file temporary yang akan disimpan di server
			$tmpName  = $_FILES['foto']['tmp_name'];
			// membaca jenis file yang diupload
			$fileType = $_FILES['foto']['type'];
			//memetakan nama direktori
			$uploadfile = $namafolder . $fileName; 
			$username = $_POST['username'];
			$password = $_POST['password'];
			// proses upload file ke folder 'data'
			if (move_uploaded_file($_FILES['foto']['tmp_name'], $uploadfile)) 
			{
				$simpan = mysql_query("INSERT INTO tbluser VALUES(NULL,'$username','$password','$fileName')");
				echo"<script>alert('User Baru Sudah Disimpan....!');location.href=\"index-admin.php?action=user\";</script>";
			} 
		}
	}
	else if($tombol=="simpan_perubahan_account")
	{
		$namafolder = 'foto/';
		if(empty($_FILES['foto']['name']))
		{
			$username = $_POST['username'];
			$password = $_POST['password'];
			$rubah = mysql_query("UPDATE tbluser SET password='$password' WHERE username='$username'");
			echo"<script>alert('Data Account Sudah Dirubah');location.href=\"index-admin.php?action=setting\";</script>";
		}
		else
		{
			$username = $_POST['username'];
			$password = $_POST['password'];
			$dbfoto = $_POST['dbfoto'];
			//hapus foto
			unlink($namafolder.$dbfoto);
			// name file
			$fileName   = $_FILES['foto']['name'];
			// nama file temporary yang akan disimpan di server
			$tmpName  = $_FILES['foto']['tmp_name'];
			// membaca jenis file yang diupload
			$fileType = $_FILES['foto']['type'];
			//memetakan nama direktori
			$uploadfile = $namafolder . $fileName; 
			$username = $_POST['username'];
			$password = $_POST['password'];
			// proses upload file ke folder 'data'
			if (move_uploaded_file($_FILES['foto']['tmp_name'], $uploadfile)) 
			{
				$rubah = mysql_query("UPDATE tbluser SET password='$password',foto='$fileName' WHERE username='$username'");
				echo"<script>alert('Data Account Sudah Dirubah');location.href=\"index-admin.php?action=setting\";</script>";
			} 
		}
	}
?>