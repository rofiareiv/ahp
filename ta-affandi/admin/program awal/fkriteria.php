<script>
	//ajax untuk tentukan semesta
	function tentukansemesta(idprefrensi)
	{
		idnya = idprefrensi;
		ajaxku = buatajax();
		var url="semesta.php";
		url=url+"?id="+idnya;
		ajaxku.onreadystatechange=stateChanged;
		ajaxku.open("GET",url,true);
		ajaxku.send(null);
	}
	function buatajax()
	{
		if (window.XMLHttpRequest)
		{
			return new XMLHttpRequest();
		}
		if (window.ActiveXObject)
		{
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
		return null;
	}
	function stateChanged()
	{
		var data;
		if (ajaxku.readyState==4)
		{
			data=ajaxku.responseText;
			if(data.length>0)
			{
				document.getElementById("inputsemesta").innerHTML = data;
			}
			else
			{
				document.getElementById("inputsemesta").value = "No Semesta";
			}
		}
	}
	//ajax untuk input
	function tentukanbobot(semesta)
	{
		semestanya = semesta;
		ajaxku = buatajax2();
		var url="input.php";
		url=url+"?semesta="+semestanya;
		ajaxku.onreadystatechange=stateChanged2;
		ajaxku.open("GET",url,true);
		ajaxku.send(null);
	}
	function buatajax2()
	{
		if (window.XMLHttpRequest)
		{
			return new XMLHttpRequest();
		}
		if (window.ActiveXObject)
		{
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
		return null;
	}
	function stateChanged2()
	{
		var data;
		if (ajaxku.readyState==4)
		{
			data=ajaxku.responseText;
			if(data.length>0)
			{
				document.getElementById("bobot").innerHTML = data;
			}
			else
			{
				document.getElementById("bobot").value = "No Input";
			}
		}
	}
</script>
<form action="proses.php" method="post" class="widget-body form">
    <div class="form-group">
		<label>Nama Kriteria</label>
        <select name="kriteria" required="">
			<option value="">Pilih Nama Kriteria</option>
			<option value="Harga Obat">Harga Obat</option>
			<option value="Kecepatan Obat">Kecepatan Obat</option>
			<option value="Keamanan Obat">Keamanan Obat</option>
			<option value="Kuantitas Obat">Kuantitas Obat</option>
			<option value="Kualitas Obat">Kualitas Obat</option>
			<option value="Daya Tahan Obat">Daya Tahan Obat</option>
		</select>
    </div>
	<div class="form-group">
		<label>Bobot</label>
		<input type="number" name="bobot" required="" placeholder="Bobot Kriteria..">
    </div>
	<div>&nbsp;</div>
	<div>
        <button type="submit" class="btn btn-success" name="btnproses" value="simpan_kriteria"><i class="icon-ok"></i>&nbsp;Simpan Kriteria</button>
        <button type="reset" class="btn btn-danger"><i class=" icon-remove"></i>&nbsp;Batal</button>
    </div>
	<div>&nbsp;</div>
</form>
<form name="fdata" method="post" action="proses.php">
	<div class="table-responsive">
		<!-- .table - Uses sparkline charts-->
		<table class="table table-bordered">
			<tr>
				<th>No</th>
				<th>Nama Kriteria</th>
				<th>Nama Alias</th>
				<th>Bobot Preferensi</th>
			</tr>
			<?php
				$sql = mysql_query("SELECT * FROM tblprefrensi ORDER BY idprefrensi ASC");
				$no=0;
				while($data = mysql_fetch_array($sql))
				{
					$no++;
			?>
			<tr>
				<td><?php echo $no?></td>
				<td><?php echo $data['nama_kriteria']?></td>
				<td><?php echo $data['alias']?></td>
				<td><?php echo $data['bobot']?></td>
			</tr>
			<?php
				}
			?>
		</table><!-- /.table -->
	</div>
	<div>&nbsp;</div>
	<div>
		<button type="submit" class="btn btn-danger" name="btnproses" value="kosongkan_kriteria"><i class="icon-trash" ></i>&nbsp;Kosongkan Kriteria</button>
	</div>
</form>
<br/><br/>
<!--Data Sub Kriteria-->
<form action="proses.php" method="post">
    <div class="form-group">
		<label>Nama Kriteria</label>
		<select name="nama_kriteria" required="" id="kriteria" onchange="tentukansemesta(this.value);">
			<option value="" selected>Pilih Nama Kriteria</option>
			<?php
				$sql = mysql_query("SELECT * FROM tblprefrensi ORDER BY idprefrensi ASC");
				$no=0;
				while($data = mysql_fetch_array($sql))
				{
			?>
			<option value="<?php echo $data['idprefrensi']?>"><?php echo $data['nama_kriteria']?></option>
			<?php
				}
			?>
		</select>
    </div>
    <div class="form-group">
		<label>Semesta Pembicaraan</label>
		<div id="inputsemesta">-</div>
		<div id="bobot"></div>
    </div>
	<div>&nbsp;</div>
	<div>
        <button type="submit" class="btn btn-success" name="btnproses" value="simpan_subkriteria"><i class="icon-ok"></i>&nbsp;Simpan Sub Kriteria</button>
        <button type="reset" class="btn btn-danger"><i class="icon-remove"></i>&nbsp;Batal</button>
    </div>
	<div>&nbsp;</div>
</form>
<form name="fdata" method="post" action="proses.php">
	<div class="box-body table-responsive">
		<!-- .table - Uses sparkline charts-->
		<table id="example2" class="table table-bordered table-hover">
			<tr>
				<th>No</th>
				<th>Nama Kriteria</th>
				<th>Nama Alias</th>
				<th>Semesta Pembicaraan</th>
				<th>Bobot</th>
			</tr>
			<?php
				$sql = mysql_query("SELECT * FROM tblkriteria ORDER BY alias ASC");
				$no=0;
				while($data = mysql_fetch_array($sql))
				{
					$no++;
			?>
			<tr>
				<td><?php echo $no?></td>
				<td><?php echo $data['nama_kriteria']?></td>
				<td><?php echo $data['alias']?></td>
				<td><?php echo $data['rentang']?></td>
				<td><?php echo $data['bobot']?></td>
			</tr>
			<?php
				}
			?>
		</table><!-- /.table -->
	</div>
	<br>
	<div>
		&nbsp;&nbsp;<button type="submit" class="btn btn-danger" name="btnproses" value="kosongkan_subkriteria"><i class="icon-trash" ></i>&nbsp;Kosongkan Sub Kriteria</button>
	</div>
	<br>
</form>