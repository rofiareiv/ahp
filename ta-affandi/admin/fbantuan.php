<h4 class="widgettitle title-inverse">Untuk menambahkan data bantuan baru anda bisa melakukannya melalui form dibawah ini, Isilah data dengan lengkap dan benar...!</h4>
<br>
<form action="proses.php" method="post">
    <div class="form-group">
		<label>Nama Bantuan</label>
        <input type="text" name="nama_bantuan" required="" placeholder="Nama Bantuan" style="width:500px;">
    </div>
	<div>
        <button type="submit" class="btn btn-primary" name="btnproses" value="simpan_bantuan"><i class="iconfa-edit"></i>&nbsp;Simpan Data Bantuan</button>
        <button type="reset" class="btn btn-danger"><i class="iconfa-remove"></i>&nbsp;Batal</button>
    </div>
	<div>&nbsp;</div>
</form>
<br><br>
<form name="fdata" method="post" action="proses.php">
	<div class="table-responsive">
		<!-- .table - Uses sparkline charts-->
		<h4 class="widgettitle">Data Bantuan</h4>
		<table class="table table-bordered" id="dyntable">
			<thead>
				<tr>
					<th>Nama Bantuan</th>
					<th><i class="iconfa-check"></i></th>
				</tr>
			</thead>
			<tbody>
            <?php
				$sql = mysql_query("SELECT * FROM tbl_bantuan ORDER BY idbantuan ASC");
				$ke=0;
				while($data = mysql_fetch_array($sql))
				{
					$ke++;
			?>
			    <tr class="gradeX">
					<td>
						<input type="hidden" name="idbantuan[]"  value="<?php echo $data['idbantuan']?>">
						<input type="text" name="nama_bantuan[]"  value="<?php echo $data['nama_bantuan']?>" style="border:none;background-color:transparent;width:300px;">
					</td>
					<td><input type="checkbox" name="idbantuan_hapus[]" value="<?php echo $data['idbantuan']?>"></td>
				</tr>
			<?php
				}
			?>
			</tbody>
		</table><!-- /.table -->
	</div>
	<div>&nbsp;</div>
	<div>
		<button type="submit" class="btn btn-primary" name="btnproses" value="simpan_perubahan_bantuan"><i class="iconfa-refresh"></i>&nbsp; Simpan Perubahan Data Bantuan</button>
		<button type="submit" class="btn btn-danger" name="btnproses" value="hapus_bantuan" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Data yang Dipilih ?');"><i class="iconfa-trash" ></i>&nbsp; Hapus Data Bantuan</button>
	</div>
</form>
<br/>
<form name="fdata" method="post" action="proses.php">
	Pilih Bantuan yang Akan Diaktifkan :
	<select name="bantuan">
		<option value="" selected>Pilih Bantuan</option>
		<?php
			$sql = mysql_query("SELECT * FROM tbl_bantuan ORDER BY idbantuan ASC");
			while($data = mysql_fetch_array($sql))
			{
				if($data['aktif']==1)
				{
		?>
				<option value="<?php echo $data['idbantuan']?>" selected><?php echo $data['nama_bantuan']?></option>
		<?php
				}
				else
				{
		?>
				<option value="<?php echo $data['idbantuan']?>"><?php echo $data['nama_bantuan']?></option>
		<?php
				}
			}
		?>
	</select>
	<button type="submit" class="btn btn-primary" name="btnproses" value="aktifkan_bantuan"><i class="iconfa-cog"></i>&nbsp; Aktifkan Bantuan</button>
</form>
<br/><br/>