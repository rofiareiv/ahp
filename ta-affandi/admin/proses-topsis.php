<?php
	include"../db/koneksi.php";
	//==============kosongkan data lama======================
	$kosongkan = mysql_query("TRUNCATE TABLE tbl_normalisasi");
	$kosongkan = mysql_query("TRUNCATE TABLE tbl_hasilkali");
	$kosongkan = mysql_query("TRUNCATE TABLE tbl_solusiedial");
	$kosongkan = mysql_query("TRUNCATE TABLE tbl_jarak");
	$kosongkan = mysql_query("TRUNCATE TABLE tbl_keputusan");
	//==============mengisi tabel normalisasi=================
	$sql_kriteria = mysql_query("SELECT idkriteria FROM tbl_kriteria ORDER BY idkriteria ASC");
	while($data_kriteria = mysql_fetch_array($sql_kriteria))
	{
		$akar_bobot=0;
		$idkriteria = $data_kriteria['idkriteria'];
		//cari total dan akar setiap kriteria
		$sql_bobot = mysql_query("SELECT sum(pow(bobot,2)) as jumlah FROM view_data_obat WHERE idkriteria='$idkriteria'");
		$data_bobot = mysql_fetch_array($sql_bobot);
		$jumlah = $data_bobot['jumlah'];
		$akar_bobot = round((sqrt($jumlah)),4);
		//ambil data bobot untuk mencari nilai r
		$sql_data = mysql_query("SELECT DISTINCT(idobat),bobot FROM  view_data_obat WHERE idkriteria='$idkriteria' ORDER BY idobat ASC");
		while($data = mysql_fetch_array($sql_data))
		{
			$nilai_r=0;
			$idobat = $data['idobat'];
			$nilai_bobot = $data['bobot'];
			$nilai_r = round(($nilai_bobot / $akar_bobot),4);
			$simpan_normalisasi = mysql_query("INSERT INTO tbl_normalisasi VALUES(NULL,'$idobat','$idkriteria','$nilai_r')");
		}
	}
	//==============mengisi hasil kali=============================
	$sql_kriteria = mysql_query("SELECT idkriteria,bobot_prefrensi FROM tbl_kriteria ORDER BY idkriteria ASC");
	while($data_kriteria = mysql_fetch_array($sql_kriteria))
	{
		$idkriteria = $data_kriteria['idkriteria'];
		$bobot_prefrensi = $data_kriteria['bobot_prefrensi'];
		//ambil nilai normalisasi
		$sql_normalisasi = mysql_query("SELECT idobat,nilai_normalisasi FROM tbl_normalisasi WHERE idkriteria='$idkriteria' ORDER BY idobat ASC");
		while($data_normalisasi = mysql_fetch_array($sql_normalisasi))
		{
			$idobat = $data_normalisasi['idobat'];
			$nilai_normalisasi = $data_normalisasi['nilai_normalisasi'];
			$nilai_hasilkali = round(($nilai_normalisasi*$bobot_prefrensi),4);
			$simpan_hasilkali = mysql_query("INSERT INTO tbl_hasilkali VALUES(NULL,'$idobat','$idkriteria','$nilai_hasilkali')");
		}
	}
	//===================mengisi tabel solusi edial=====================
	$sql_kriteria = mysql_query("SELECT idkriteria FROM tbl_kriteria ORDER BY idkriteria ASC");
	while($data_kriteria = mysql_fetch_array($sql_kriteria))
	{
		$idkriteria = $data_kriteria['idkriteria'];
		//ambil data bobot min max
		$sql_solusimin = mysql_query("SELECT min(nilai_hasil_kali) as min FROM tbl_hasilkali WHERE idkriteria='$idkriteria'");
		$sql_solusimax = mysql_query("SELECT max(nilai_hasil_kali) as max FROM tbl_hasilkali WHERE idkriteria='$idkriteria'");
		$data_min = mysql_fetch_array($sql_solusimin);
		$data_max = mysql_fetch_array($sql_solusimax);
		$s_negatif = $data_min['min'];
		$s_positif = $data_max['max'];
		$simpan_solusi = mysql_query("INSERT INTO tbl_solusiedial VALUES(NULL,'Negatif','$idkriteria','$s_negatif')");
		$simpan_solusi = mysql_query("INSERT INTO tbl_solusiedial VALUES(NULL,'Positif','$idkriteria','$s_positif')");
	}
	//=======================================mengisi tabel jarak solusi edial================================
	$sql_penduduk = mysql_query("SELECT DISTINCT(idobat) FROM tbl_obat ORDER BY idobat ASC");
	while($data_penduduk = mysql_fetch_array($sql_penduduk))
	{
		$idobat = $data_penduduk['idobat'];
		$jum_JP=0;
		$jum_JN=0;
		$jarak_P=0;
		$jarak_N=0;
		//ambil data hasil kali
		$sql_jarak = mysql_query("SELECT idkriteria,nilai_hasil_kali FROM tbl_hasilkali WHERE idobat='$idobat' ORDER BY idkriteria ASC");
		while($data_jarak = mysql_fetch_array($sql_jarak))
		{
			$idkriteria = $data_jarak['idkriteria'];
			$nilai_hasilkali = $data_jarak['nilai_hasil_kali'];
			//ambil nilai solusi edial positif
			$sql_solusiP = mysql_query("SELECT nilai_solusi FROM tbl_solusiedial WHERE edial='Positif' AND idkriteria='$idkriteria'");
			$data_solusiP = mysql_fetch_array($sql_solusiP);
			$solusi_positif = $data_solusiP['nilai_solusi'];
			//ambil nilai solusi edial negatif
			$sql_solusiN = mysql_query("SELECT nilai_solusi FROM tbl_solusiedial WHERE edial='Negatif' AND idkriteria='$idkriteria'");
			$data_solusiN = mysql_fetch_array($sql_solusiN);
			$solusi_negatif = $data_solusiN['nilai_solusi'];
			//menghitung nilai jarak positif dan negatif
			$jum_JP += pow(($nilai_hasilkali - $solusi_positif),2);
			$jum_JN += pow(($nilai_hasilkali - $solusi_negatif),2);
		}
		//hitung jarak edial positif dan negatif
		$jarak_P = round(sqrt($jum_JP),4);
		$jarak_N = round(sqrt($jum_JN),4);
		$simpan_jarak = mysql_query("INSERT INTO tbl_jarak VALUES(NULL,'$idobat','$jarak_P','$jarak_N')");
	}
	//=================================proses hitung keputusan==================================
	$sql = mysql_query("SELECT * FROM tbl_jarak ORDER BY idobat ASC");
	while($data = mysql_fetch_array($sql))
	{
		$idobat = $data['idobat'];
		$SP = $data['dpositif'];
		$SN = $data['dnegatif'];
		$NA = round(($SN/($SN+$SP)),4);
		//simpan keputusan
		$simpan = mysql_query("INSERT INTO tbl_keputusan VALUES(NULL,'$idobat','$NA')");
	}
	echo"<script>alert('Proses Perhitungan Metode Topsis Selesai..!');location.href=\"index-admin.php?action=keputusan\";</script>";
?>