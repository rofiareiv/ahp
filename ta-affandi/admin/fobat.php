<h5 class="widgettitle title-inverse">Untuk menambahkan data obat baru, anda bisa melakukannya melalui form dibawah ini, Isilah data dengan lengkap dan benar...!</h5>
<br>
<form action="proses.php" method="post">
	<div class="form-group">
		<label>Pilih Hama</label>
		<select class="form-control" name="idhama" required="" style="width:340px;">
			<option value="" selected>Pilih Nama Hama</option>
			<?php
				$sql_hama = mysql_query("SELECT * FROM tbl_hama ORDER BY idhama ASC");
				while($data_hama = mysql_fetch_array($sql_hama))
				{
			?>
					<option value="<?php echo $data_hama['idhama']?>"><?php echo $data_hama['nama_hama']?></option>
			<?php
				}
			?>
		</select>
    </div> 
	<div class="form-group">
		<label>Nama Obat</label>
		<input type="text" class="form-control" name="nama_obat" required="" style="width:340px;">
    </div>
	<?php
		$sql_kriteria = mysql_query("SELECT * FROM tbl_kriteria ORDER BY idkriteria ASC");
		while($data_kriteria = mysql_fetch_array($sql_kriteria))
		{
			$idkriteria = $data_kriteria['idkriteria'];
	?>
	<div class="form-group">
		<label><?php echo $data_kriteria['nama_kriteria']?></label>
		<select name="idsubkriteria<?php echo $idkriteria?>" style="width:340px;" required="">
			<option value="" selected>Pilih Salah Satu Opsi</option>
			<?php
				$sql_subkriteria = mysql_query("SELECT * FROM tbl_subkriteria WHERE idkriteria='$idkriteria'");
				while($data_subkriteria = mysql_fetch_array($sql_subkriteria))
				{
			?>
			 <option value="<?php echo $data_subkriteria['idsubkriteria']?>"><?php echo $data_subkriteria['subkriteria']?></option>
			<?php
				}
			?>
		</select>
	</div>
	<?php
		}
	?>
	<div>&nbsp;</div>
	<div>
        <button class="btn btn-primary" type="submit" name="btnproses" value="simpan_obat"><span class="icon-ok"></span>&nbsp;Simpan Data Obat</button>
        <button type="reset" class="btn btn-danger"><span class="icon-remove"></span>&nbsp;Batal</button>
    </div>
</form>
<br>
<!--Data kriteria-->
<form name="myform" method="post" action="proses.php">
	<div class="table-responsive">
		<h4 class="widgettitle">Data Obat</h4>
		<table class="table table-bordered" id="dyntable">
			<thead>
				<tr>
					<th style="font-size:11px;">No</th>
					<th style="font-size:11px;">Nama Hama</th>
					<th style="font-size:11px;">Nama Obat</th>
					<?php
						$sql_kriteria = mysql_query("SELECT * FROM tbl_kriteria ORDER BY idkriteria ASC");
						while($data_kriteria = mysql_fetch_array($sql_kriteria))
						{
					?>
					<th style="font-size:11px;text-align:center;"><?php echo $data_kriteria['nama_kriteria']?></th>
					<?php
						}
					?>
					<th><i class="icon-check"></i></th>
				</tr>
			</thead>
			<tbody>
				<?php
					$sql_obat = mysql_query("SELECT DISTINCT(idobat),nama_obat FROM view_data_obat");
					$no=0;
					while($data_obat = mysql_fetch_array($sql_obat))
					{	
						$no++;
						$idobat = $data_obat['idobat'];
						$sql_idhama = mysql_query("SELECT idhama FROM tbl_obat WHERE idobat='$idobat'");
						$data_idhama= mysql_fetch_array($sql_idhama);
						$idhama = $data_idhama['idhama'];
						$sql_hama = mysql_query("SELECT * FROM tbl_hama WHERE idhama='$idhama'");
						$data_hama= mysql_fetch_array($sql_hama);
				?>
				<tr>
					<td><?php echo $no?></td>
					<td><?php echo $data_hama['nama_hama']?></td>
					<td><?php echo $data_obat['nama_obat']?></td>
					<?php
						$sql_data = mysql_query("SELECT subkriteria FROM view_data_obat WHERE idobat='$idobat' ORDER BY idkriteria ASC");
						while($data = mysql_fetch_array($sql_data))
						{
					?>
					<td><?php echo $data['subkriteria']?></td>
					<?php
						}
					?>
					<td><input type="checkbox" name="idobat[]" value="<?php echo $idobat?>"></td>
				</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
	<div>&nbsp;</div>
	<div>
		<button type="submit" class="btn btn-danger" name="btnproses" value="hapus_data_obat" onclick="return confirm('Yakin Data obat Penerima Bantuan Terpilih Ingin Dihapus ??');"><i class="icon-remove"></i>&nbsp;Hapus Data Obat</button>
		<button type="submit" class="btn btn-danger" name="btnproses" value="kosongkan_data_obat" onclick="return confirm('Yakin Semua Data obat Penerima Bantuan Ingin Dihapus ??');"><i class="icon-trash"></i>&nbsp;Kosongkan Data Obat</button>
	</div>
</form>
<br>
<!--Data Bobot Kecocokan-->
<div class="table-responsive">
	<h4 class="widgettitle">Data Bobot obat</h4>
	<table class="table table-bordered" id="dyntable1">
		<thead>
			<tr>
				<th style="font-size:11px;">No</th>
				<th style="font-size:11px;">Nama Hama</th>
				<th style="font-size:11px;">Nama obat</th>
				<?php
					$sql_kriteria = mysql_query("SELECT * FROM tbl_kriteria ORDER BY idkriteria ASC");
					while($data_kriteria = mysql_fetch_array($sql_kriteria))
					{
				?>
				<th style="font-size:11px;text-align:center;"><?php echo $data_kriteria['nama_kriteria']?></th>
				<?php
					}
				?>
			</tr>
		</thead>
		<tbody>
			<?php
				$sql_obat = mysql_query("SELECT DISTINCT(idobat),nama_obat FROM view_data_obat");
				$no=0;
				while($data_obat = mysql_fetch_array($sql_obat))
				{
					$no++;
					$idobat = $data_obat['idobat'];
					$sql_idhama = mysql_query("SELECT idhama FROM tbl_obat WHERE idobat='$idobat'");
					$data_idhama= mysql_fetch_array($sql_idhama);
					$idhama = $data_idhama['idhama'];
					$sql_hama = mysql_query("SELECT * FROM tbl_hama WHERE idhama='$idhama'");
					$data_hama= mysql_fetch_array($sql_hama);
			?>
			<tr>
				<td><?php echo $no?></td>
				<td><?php echo $data_hama['nama_hama']?></td>
				<td><?php echo $data_obat['nama_obat']?></td>
				<?php
					$sql_data = mysql_query("SELECT bobot FROM view_data_obat WHERE idobat='$idobat' ORDER BY idkriteria ASC");
					while($data = mysql_fetch_array($sql_data))
					{
				?>
				<td><?php echo $data['bobot']?></td>
				<?php
					}
				?>
			</tr>
			<?php
				}
			?>
		</tbody>
	</table>
	<br><br>
</div>