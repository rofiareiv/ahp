<script>
	//ajax untuk input
	function tentukanbobot(semesta)
	{
		semestanya = semesta;
		ajaxku = buatajax2();
		var url="bobot.php";
		ajaxku.onreadystatechange=stateChanged2;
		ajaxku.open("GET",url,true);
		ajaxku.send(null);
	}
	function buatajax2()
	{
		if (window.XMLHttpRequest)
		{
			return new XMLHttpRequest();
		}
		if (window.ActiveXObject)
		{
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
		return null;
	}
	function stateChanged2()
	{
		var data;
		if (ajaxku.readyState==4)
		{
			data=ajaxku.responseText;
			if(data.length>0)
			{
				document.getElementById("subkriteria").innerHTML = data;
			}
		}
	}
</script>
<h5 class="widgettitle title-inverse">Untuk menambahkan data kriteria baru untuk salah satu bantuan anda bisa melakukannya melalui form dibawah ini, Isilah data dengan lengkap dan benar...!</h5>
<br>
<form action="proses.php" method="post">
    <div class="form-group">
		<label>Nama Kriteria</label>
        <input type="text" name="nama_kriteria" required="" placeholder="Nama Kriteria" style="width:350px;" autofocus>
    </div>
	<div class="form-group">
		<label>Bobot Prefrensi</label>
		<input type="number" name="bobot_prefrensi" required="" placeholder="Bobot Prefrensi Kriteria">
    </div>
	<div>&nbsp;</div>
	<div>
        <button type="submit" class="btn btn-primary" name="btnproses" value="simpan_kriteria"><i class="icon-ok"></i>&nbsp;Simpan Kriteria</button>
        <button type="reset" class="btn btn-danger"><i class="icon-remove"></i>&nbsp;Batal</button>
    </div>
	<div>&nbsp;</div>
</form>
<br>
<form name="fdata" method="post" action="proses.php">
	<div class="table-responsive">
		<h4 class="widgettitle">Data Kriteria</h4>
		<table class="table table-bordered" id="dyntable">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Kriteria</th>
					<th>Bobot Preferensi</th>
					<th><i class="icon-check"></i></th>
				</tr>
			</thead>
			<tbody>
			<?php
				$sql = mysql_query("SELECT * FROM tbl_kriteria ORDER BY idkriteria ASC");
				$no=0;
				while($data = mysql_fetch_array($sql))
				{
					$no++;
			?>
				<tr>					
					<td>
						<?php echo $no?>
						<input type="hidden" name="idkriteria[]" value="<?php echo $data['idkriteria']?>">
					</td>
					<td><input type="text" name="nama_kriteria[]" value="<?php echo $data['nama_kriteria']?>" style="border:none;background-color:transparent;width:350px;" required=""></td>
					<td><input type="number" name="bobot_prefrensi[]" value="<?php echo $data['bobot_prefrensi']?>" style="border:none;background-color:transparent;width:70px;" required=""></td>
					<td><input type="checkbox" name="idkriteria_hapus[]" value="<?php echo $data['idkriteria']?>"></td>
				</tr>
			<?php
				}
			?>
			</tbody>
		</table><!-- /.table -->
	</div>
	<div>&nbsp;</div>
	<div>
		<button type="submit" class="btn btn-primary" name="btnproses" value="simpan_perubahan_kriteria"><i class="icon-refresh"></i>&nbsp; Simpan Perubahan Kriteria</button>
		<button type="submit" class="btn btn-danger" name="btnproses" value="hapus_kriteria" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Data Kriteria yang Dipilih ?');"><i class="icon-trash"></i>&nbsp; Hapus Kriteria</button>
	</div>
</form>
<br/><hr/>
<!--Data Sub Kriteria-->
<h5 class="widgettitle title-inverse">Untuk menambahkan data subkriteria baru untuk salah satu bantuan anda bisa melakukannya melalui form dibawah ini, Isilah data dengan lengkap dan benar...!</h5>
<br>
<form action="proses.php" method="post">
	<div class="form-group">
		<label>Pilih Kriteria</label>
		<select name="idkriteria" required="" style="width:250px;" id="bantuan" onchange="tentukanbobot(this.value);" >
			<option value="" selected>Pilih Kriteria</option>
			<?php
				$sql_kriteria = mysql_query("SELECT * FROM tbl_kriteria ORDER BY idkriteria ASC");
				while($data_kriteria = mysql_fetch_array($sql_kriteria))
				{
			?>
			<option value="<?php echo $data_kriteria['idkriteria']?>"><?php echo $data_kriteria['nama_kriteria']?></option>
			<?php
				}
			?>
		</select>
    </div>
	<div class="form-group">
		<label>Subkriteria</label>
		<span id="subkriteria">-</span>
    </div>
	<div>&nbsp;</div>
	<div>
        <button type="submit" class="btn btn-primary" name="btnproses" value="simpan_subkriteria"><i class="icon-edit"></i>&nbsp;Simpan Subkriteria</button>
        <button type="reset" class="btn btn-danger"><i class="icon-remove"></i>&nbsp;Batal</button>
    </div>
	<div>&nbsp;</div>
</form>
<form name="fdata" method="post" action="proses.php">
	<div class="box-body table-responsive">
		<h4 class="widgettitle">Data Subkriteria</h4>
		<table class="table table-bordered" id="dyntable1">	
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Kriteria</th>
					<th>Subkriteria</th>
					<th>Bobot</th>
					<th><i class="icon-check"></i></th>
				</tr>
			</thead>
			<tbody>
			<?php
				$sql = mysql_query("SELECT * FROM tbl_subkriteria ORDER BY idkriteria ASC");
				$no=0;
				while($data = mysql_fetch_array($sql))
				{
					$no++;
					$idkriteria = $data['idkriteria'];
					$sql_namakriteria = mysql_query("SELECT * FROM tbl_kriteria WHERE idkriteria='$idkriteria'");
					$data_namakriteria= mysql_fetch_array($sql_namakriteria);
					$nama_kriteria = $data_namakriteria['nama_kriteria'];
			?>
			<tr>
				<td><?php echo $no?></td>
				<td>
					<?php echo $nama_kriteria?>
					<input type="hidden" name="idsubkriteria[]" value="<?php echo $data['idsubkriteria']?>">
				</td>
				<td><input type="text" name="subkriteria[]" value="<?php echo $data['subkriteria']?>" style="border:none;background-color:transparent;width:250px;" required=""></td>
				<td><input type="number" name="bobot[]" value="<?php echo $data['bobot']?>" style="border:none;background-color:transparent;width:70px;" required=""></td>
				<td><input type="checkbox" name="idsubkriteria_hapus[]" value="<?php echo $data['idsubkriteria']?>"></td>
			</tr>
			<?php
				}
			?>
			</tbody>
		</table><!-- /.table -->
	</div>
	<div>&nbsp;</div>
	<div>
		<button type="submit" class="btn btn-primary" name="btnproses" value="simpan_perubahan_subkriteria"><i class="icon-refresh"></i>&nbsp; Simpan Perubahan Subkriteria</button>
		<button type="submit" class="btn btn-danger" name="btnproses" value="hapus_subkriteria" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Data Subkriteria yang Dipilih ?');"><i class="icon-trash"></i>&nbsp; Hapus Subkriteria</button>
	</div>
	<div>&nbsp;</div>
</form>