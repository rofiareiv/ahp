<div class="table-responsive">
	<h4 class="widgettitle">Data Obat</h4>
	<table class="table table-bordered" id="dyntable">
		<thead>
			<tr>
				<th style="font-size:11px;">No</th>
				<th style="font-size:11px;">Data Obat</th>
				<?php
					$sql_kriteria = mysql_query("SELECT * FROM tbl_kriteria  ORDER BY idkriteria ASC");
					while($data_kriteria = mysql_fetch_array($sql_kriteria))
					{
				?>
				<th style="font-size:11px;text-align:center;"><?php echo $data_kriteria['nama_kriteria']?></th>
				<?php
					}
				?>
			</tr>
		</thead>
		<tbody>
			<?php
				$sql_obat = mysql_query("SELECT DISTINCT(idobat),nama_obat FROM view_data_obat ORDER BY nama_obat ASC");
				$no=0;
				while($data_obat = mysql_fetch_array($sql_obat))
				{	
					$no++;
					$idobat = $data_obat['idobat'];
			?>
			<tr>
				<td><?php echo $no?></td>
				<td><?php echo $data_obat['nama_obat']?></td>
				<?php
					$sql_data = mysql_query("SELECT subkriteria FROM view_data_obat WHERE idobat='$idobat' ORDER BY idkriteria ASC");
					while($data = mysql_fetch_array($sql_data))
					{
				?>
				<td><?php echo $data['subkriteria']?></td>
				<?php
					}
				?>
			</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>
<div>&nbsp;</div>
<!--Data pembobotan-->
<div class="table-responsive">
	<h4 class="widgettitle">Data Bobot obat</h4>
	<table class="table table-bordered" id="dyntable1">
		<thead>
			<tr>
				<th style="font-size:11px;">No</th>
				<th style="font-size:11px;">Nama Obat</th>
				<?php
					$sql_kriteria = mysql_query("SELECT * FROM tbl_kriteria ORDER BY idkriteria ASC");
					while($data_kriteria = mysql_fetch_array($sql_kriteria))
					{
				?>
				<th style="font-size:11px;text-align:center;"><?php echo $data_kriteria['nama_kriteria']?></th>
				<?php
					}
				?>
			</tr>
		</thead>
		<tbody>
			<?php
				$sql_obat = mysql_query("SELECT DISTINCT(idobat),nama_obat FROM view_data_obat ORDER BY nama_obat ASC");
				$no=0;
				while($data_obat = mysql_fetch_array($sql_obat))
				{	
					$no++;
					$idobat = $data_obat['idobat'];
			?>
			<tr>
				<td><?php echo $no?></td>
				<td><?php echo $data_obat['nama_obat']?></td>
				<?php
					$sql_data = mysql_query("SELECT bobot FROM view_data_obat WHERE idobat='$idobat' ORDER BY idkriteria ASC");
					while($data = mysql_fetch_array($sql_data))
					{
				?>
				<td><?php echo $data['bobot']?></td>
				<?php
					}
				?>
			</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>
<div>&nbsp;</div>
<!--Data hasil normalisasi-->
<div class="table-responsive">
	<h4 class="widgettitle">Hasil Normaliasi Nilai Bobot Data obat</h4>
	<table class="table table-bordered" id="dyntable3">
		<thead>
			<tr>
				<th style="font-size:11px;">Nama obat</th>
				<?php
					$sql_kriteria = mysql_query("SELECT * FROM tbl_kriteria ORDER BY idkriteria ASC");
					while($data_kriteria = mysql_fetch_array($sql_kriteria))
					{
				?>
				<th style="font-size:11px;text-align:center;"><?php echo $data_kriteria['nama_kriteria']." (".$data_kriteria['bobot_prefrensi'].")"?></th>
				<?php
					}
				?>
			</tr>
		</thead>
		<tbody>
			<?php
				$sql_normalisasi = mysql_query("SELECT DISTINCT(idobat),nama_obat FROM view_data_obat ORDER BY nama_obat ASC");
				while($data_normalisasi = mysql_fetch_array($sql_normalisasi))
				{	
					$idobat = $data_normalisasi['idobat'];
			?>
			<tr>
				<td><?php echo $data_normalisasi['nama_obat']?></td>
				<?php
					$sql_normalisasi1 = mysql_query("SELECT nilai_normalisasi FROM tbl_normalisasi WHERE idobat='$idobat' ORDER BY idkriteria ASC");
					while($data_normalisasi1 = mysql_fetch_array($sql_normalisasi1))
					{
				?>
				<td><?php echo $data_normalisasi1['nilai_normalisasi']?></td>
				<?php
					}
				?>
			</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>
<div>&nbsp;</div>
<!--Data hasil normalisasi-->
<div class="table-responsive">
	<h4 class="widgettitle">Hasil Perkalian Nilai Normaliasi dengan Bobot Prefrensi</h4>
	<table class="table table-bordered" id="dyntable4">
		<thead>
			<tr>
				<th style="font-size:11px;">Nama obat</th>
				<?php
					$sql_kriteria = mysql_query("SELECT * FROM tbl_kriteria ORDER BY idkriteria ASC");
					while($data_kriteria = mysql_fetch_array($sql_kriteria))
					{
				?>
				<th style="font-size:11px;text-align:center;"><?php echo $data_kriteria['nama_kriteria']?></th>
				<?php
					}
				?>
			</tr>
		</thead>
		<tbody>
			<?php
				$sql_data = mysql_query("SELECT DISTINCT(idobat),nama_obat FROM view_data_obat ORDER BY nama_obat ASC");
				while($data_obat = mysql_fetch_array($sql_data))
				{	
					$idobat = $data_obat['idobat'];
			?>
			<tr>
				<td><?php echo $data_obat['nama_obat']?></td>
				<?php
					$sql_hasilkali = mysql_query("SELECT nilai_hasil_kali FROM tbl_hasilkali WHERE idobat='$idobat' ORDER BY idkriteria ASC");
					while($data_hasilkali = mysql_fetch_array($sql_hasilkali))
					{
				?>
				<td><?php echo $data_hasilkali['nilai_hasil_kali']?></td>
				<?php
					}
				?>
			</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>
<div>&nbsp;</div>
<div class="table-responsive">
	<h4 class="widgettitle">Nilai Solusi Edial Positif dan Negarif</h4>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th class="head0">Solusi Edial</th>
				<?php
					$sql = mysql_query("SELECT * FROM tbl_kriteria ORDER BY idkriteria ASC");
					while($data = mysql_fetch_array($sql))
					{
				?>
				<th class="head0" align="center"><?php echo $data['nama_kriteria']?></th>
				<?php
					}
				?>
			</tr>
		</thead>
		<tbody>
			<?php
				$sql = mysql_query("SELECT DISTINCT(edial) FROM tbl_solusiedial ORDER BY idsolusi ASC");
				while($data = mysql_fetch_array($sql))
				{
					$edial = $data['edial'];
					if($edial=="Negatif")
						$tanda="A-";
					else
						$tanda="A+";
			?>
			<tr class="gradeX">
				<td><?php echo $edial."($tanda)"?></td>
				<?php
					$sql_solusi = mysql_query("SELECT * FROM tbl_solusiedial WHERE edial='$edial' ORDER BY idkriteria ASC");
					while($data_solusi = mysql_fetch_array($sql_solusi))
					{
				?>
				<td><?php echo $data_solusi['nilai_solusi']?></td>
				<?php
					}
				?>
			</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>
<div>&nbsp;</div>
<div class="table-responsive">
	<h4 class="widgettitle">Nilai Jarak Solusi Edial Positif dan Negatif</h4>
	<table class="table table-bordered" id="dyntable5">
		<thead>
			<tr>
				<th class="head0">Nama obat </th>
				<th class="head0">Jarak Solusi Edial Positif (S+)</th>
				<th class="head0">Jarak Solusi Edial Negatif (S-)</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$sql_jarak = mysql_query("SELECT * FROM tbl_jarak ORDER BY idobat ASC");
				while($data_jarak = mysql_fetch_array($sql_jarak))
				{
					$idobat = $data_jarak['idobat'];
					$sql_penduduk = mysql_query("SELECT * FROM tbl_obat WHERE idobat='$idobat'");
					$data_penduduk = mysql_fetch_array($sql_penduduk);
					$nama_obat = $data_penduduk['nama_obat'];
			?>
			<tr class="gradeX">
				<td><?php echo $nama_obat?></td>
				<td><?php echo $data_jarak['dpositif']?></td>
				<td><?php echo $data_jarak['dnegatif']?></td>
			</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>
<div>&nbsp;</div>
<div class="table-responsive">
	<h4 class="widgettitle">Rekomendasi Pemilihan Obat</h4>
	<table class="table table-bordered" id="dyntable6">
		<thead>
			<tr>
				<th class="head0">No</th>
				<th class="head0">Nama Hama</th>
				<th class="head0">Nama obat</th>
				<th class="head0">Nilai</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$sql = mysql_query("SELECT * FROM tbl_keputusan ORDER BY na DESC");
				$no=0;
				while($data = mysql_fetch_array($sql))
				{
					$no++;
					$idobat = $data['idobat'];
					$sql_obat = mysql_query("SELECT * FROM tbl_obat WHERE idobat='$idobat'");
					$data_obat= mysql_fetch_array($sql_obat);
					$idhama = $data_obat['idhama'];
					$sql_hama = mysql_query("SELECT * FROM tbl_hama WHERE idhama='$idhama'");
					$data_hama= mysql_fetch_array($sql_hama);
			?>
			<tr class="gradeX">
				<td><?php echo $no?></td>
				<td><?php echo $data_hama['nama_hama']?></td>
				<td><?php echo $data_obat['nama_obat']?></td>
				<td><?php echo $data['na']?></td>
			</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</div>
<p style="color:red;font-weight:bold;font-size:15px">Keterangan : Semakin besar nilai yang diperoleh maka obat semakin disarankan untuk digunakan mengatasi hama dimaksud.</p>
<form name="fbaru" method="post" action="proses.php">
	<button type="button" class="btn btn-primary" onclick="window.open('cetak-keputusan.php','new','width=1100,height=650,menubars=no');"><span class="icon-print"></span>&nbsp;Cetak Hasil Keputusan</button>
	<button type="submit" class="btn btn-default" name="btnproses" value="buat_seleksi_baru"><span class="icon-edit"></span>&nbsp;Buat Seleksi Baru</button>
</form>
<br><br>