<h4 class="widgettitle title-inverse">Untuk menambahkan data penduduk baru anda bisa melakukannya melalui form dibawah ini, Isilah data dengan lengkap dan benar...!</h4>
<br>
<form action="proses.php" method="post">
	<div class="form-group">
		<label>No.KTP</label>
        <input type="text" name="no_ktp" required="" maxlength="16" placeholder="Nomor KTP" style="width:300px;">
    </div>
    <div class="form-group">
		<label>Nama Kepala Keluarga</label>
        <input type="text" name="nama_kk" required="" placeholder="Nama Kepala Keluarga" style="width:500px;">
    </div>
	<div class="form-group">
		<label>Berasal dari Dusun</label>
		<select name="dusun" required="" style="width:350px;">
			<option value="">Pilih Dusun</option>
			<option value="Embung Barang Tengah">Embung Barat Tengah</option>
			<option value="Embung Barat Utara">Embung Barat Utara</option>
			<option value="Embung Barat Selatan">Embung Barat Selatan</option>
			<option value="Angsana Tengah">Angsana Tengah</option>
			<option value="Angsana Timur">Angsana Timur</option>
			<option value="Angsana Barat">Angsana Barat</option>
			<option value="Jalinan Timur">Jalinan Timur</option>
			<option value="Jalinan Barat">Jalinan Barat</option>
			<option value="Jalinan Tengah">Jalinan Tengah</option>
			<option value="Lekoh Barat">Lekoh Barat</option>
			<option value="Lekoh Timur">Lekoh Timur</option>
			<option value="Berkongan Utara">Berkongan Utara</option>
			<option value="Berkongan Selatan">Berkongan Selatan</option>
		</select>
	</div>
	<div>
        <button type="submit" class="btn btn-primary" name="btnproses" value="simpan_penduduk"><i class="iconfa-edit"></i>&nbsp;Simpan Data Penduduk</button>
        <button type="reset" class="btn btn-danger"><i class="iconfa-remove"></i>&nbsp;Batal</button>
    </div>
	<div>&nbsp;</div>
</form>
<br><br>
<form name="fdata" method="post" action="proses.php">
	<div class="table-responsive">
		<!-- .table - Uses sparkline charts-->
		<h4 class="widgettitle">Data Penduduk Pada Setiap Dusun</h4>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>Nomor KTP</th>
					<th>Nama Kepala Keluarga</th>
					<th>Asal Dusun</th>
					<th><i class="iconfa-check"></i></th>
				</tr>
			</thead>
			<tbody>
            <?php
				$sql = mysql_query("SELECT * FROM tbl_penduduk ORDER BY id_penduduk DESC");
				while($data = mysql_fetch_array($sql))
				{
			?>
			    <tr class="gradeX">
					<td>
						<input type="hidden" name="no_ktp[]"  value="<?php echo $data['no_ktp']?>">
						<?php echo $data['no_ktp']?>
					</td>
					<td>
						<input type="text" name="nama_kk[]"  value="<?php echo $data['nama_kk']?>" style="border:none;background-color:transparent;width:300px;">
					</td>
					<td><input type="text" name="dusun[]" readonly value="<?php echo $data['dusun']?>" style="border:none;background-color:transparent;width:300px;"></td>
					<td><input type="checkbox" name="no_ktp_hapus[]" value="<?php echo $data['no_ktp']?>"></td>
				</tr>
			<?php
				}
			?>
			</tbody>
		</table><!-- /.table -->
	</div>
	<div>&nbsp;</div>
	<div>
		<button type="submit" class="btn btn-primary" name="btnproses" value="simpan_perubahan_penduduk"><i class="iconfa-refresh"></i>&nbsp; Simpan Perubahan Data Penduduk</button>
		<button type="submit" class="btn btn-danger" name="btnproses" value="hapus_penduduk" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Data yang Dipilih ?');"><i class="iconfa-trash" ></i>&nbsp; Hapus Data Penduduk</button>
	</div>
</form>
<br/><br/>