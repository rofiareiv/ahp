<center>
	<font color="green" size="5" face="Cambria,Tahoma">Pentunjuk Penggunaan Aplikasi</font>
</center>
<ol style="color:#F33;font-family:Cambria,Calibri,Tahoma;font-size:18px;text-align:justify">
	<li>Entry Data Dosen</li>
		<p>Untuk melakukan entry data dosen silahkan anda mengikuti langkah-langkah berikut:</p>
		<ul>
			<li>Input data dosen dengan lengkap dan benar kemudian klik tombol "Simpan Data Dosen"</li>
			<li>Manage data dosen digunakan untuk melakukan perubahan data dosen jika ada data yang salah,lakukan perubahan seperlunya dan klik tombol "Simpan Perubahan" untuk menyimpan hasil perubahan, dan klik tombol "Hapus Data" untuk menghapus data dosen namun sebelumnya silahkan pilih data dosen yang akan dihapus.</li>
			<li>Setiap dosen yang diinputkan secara otomatis akan memiliki account dengan username [NIS] dan passwordnya[nn].</li>
			<li>Dosen yang sudah dientrykan secara otomatis akan masuk ke rekap gaji dosen.</li>
		</ul> 
		<br>
	<li>Entry Data Mahasiswa</li>
		<p>Untuk melakukan entry data mahasiswa ikuti langkah berikut:</p>
		<ul>
			<li>Input data mahasiswa dengan lengkap dan benar kemudian klik tombol "Simpan Data Mahasiswa"</li>
			<li>Manage data mahasiswa digunakan untuk melakukan perubahan data dosen jika ada data yang salah,lakukan perubahan seperlunya dan klik tombol "Simpan Perubahan" untuk menyimpan hasil perubahan, dan klik tombol "Hapus Data" untuk menghapus data mahasiswa namun sebelumnya silahkan pilih data mahasiswa yang akan dihapus.</li>
			<li>Setiap mahasiswa yang diinputkan secara otomatis akan memiliki account dengan username [NIM] dan passwordnya[nn].</li>
			<li>Mahasiswa yang sudah dientrykan secara otomatis akan masuk ke absensi mahasiswa.</li>
		</ul>
		<br>
	<li>Entry Data Matakuliah</li>
		<p>Untuk melakukan entry data matakuliah ikuti langkah berikut :</p>
		<ul>
			<li>Input data matakuliah dengan lengkap dan benar kemudian klik tombol "Simpan Data Matakuliah"</li>
			<li>Jika ada data yang salah,lakukan perubahan seperlunya dan klik tombol "Simpan Perubahan" untuk menyimpan hasil perubahan, dan klik tombol "Hapus Data" untuk menghapus data matakuliah namun sebelumnya silahkan pilih data matakuliah yang akan dihapus.</li>
		</ul>
		<br>
	<li>Data Kampus</li>
		<p>Menu ini digunakan untuk management data fakultas dan data jurusan</p>
		<br>
	<li>Entry Data Nilai Mahasiswa</li>
		<p>Untuk melakukan entry data nilai mahasiswa ikuti langkah berikut :</p>
		<ul>
			<li>Pilih Semesternya,kemudian pilih matakuliah yang ada disemester itu,kemudian pilih angkatan mahasiswanya,setelah itu akan dimunculkan semua daftar mahasiswa yang ada pada angkatan tersebut berikut isian nilainya.</li>
			<li>Silahkan diisi nilai mahasiswa pada matakuliah dan semester yang dipilih kemudian klik tombol "Simpan Nilai Mahasiswa".</li>
		</ul>
		<br>
	<li>Setting Account</li>
	<p>Untuk mensetting data account klik menu "Setting Account" lakukan perubahan data account seperlunya dan dilakukan secara berkala untuk alasan keamanan</p>
</ol>