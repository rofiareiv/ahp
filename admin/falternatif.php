<style>
	th
	{
		color:#033;
		font-family:Cambria,Verdana;
		font-size:14px;
	}
</style>
<?php
	if(!empty($_SESSION['iduser']) && !empty($_SESSION['username']) && !empty($_SESSION['password']))
	{
?>
<SCRIPT TYPE="text/javascript">
function namesonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which;
	else
		return true;
	keychar = String.fromCharCode(key);
	// control keys
	if ((key==null) || (key==0) || (key==8) ||(key==9) || (key==13) || (key==27) )
		return true;
	// numbers
	else if (((" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`'").indexOf(keychar) > -1))
		return true;
	else
		return false;
}
</SCRIPT>
<script>
function pilihan()
	{
     // membaca jumlah komponen dalam form bernama 'myform'
     var jumKomponen = document.myform.length;

     // jika checkbox 'Pilih Semua' dipilih
     if (document.myform[0].checked == true)
     {
        // semua checkbox pada data akan terpilih
        for (i=1; i<=jumKomponen; i++)
        {
            if (document.myform[i].type == "checkbox") document.myform[i].checked = true;
        }
     }
     // jika checkbox 'Pilih Semua' tidak dipilih
     else if (document.myform[0].checked == false)
        {
            // semua checkbox pada data tidak dipilih
            for (i=1; i<=jumKomponen; i++)
            {
               if (document.myform[i].type == "checkbox") document.myform[i].checked = false;
            }
        }
  }
</script>
<form name="falternatif" method="post" action="proses.php">
	<table cellpadding="5">
		<tr>
			<td><b>Nama Kandidat</b></td>
			<td><b>:</b></td>
			<td><input  type="text" name="nama_kandidat" required="" placeholder="Nama Kandidat..." autofocus style="width:400px" onKeyPress="return namesonly(this, event)"/><font color="red"><b> * Harus Huruf</b></font></td>
		</tr>
		<tr>
			<td><b>Nama Alias</b></td>
			<td><b>:</b></td>
			<td><input  type="text" name="nama_alias" required="" placeholder="Nama Alias..." autofocus style="width:200px" onKeyPress="return namesonly(this, event)" maxlength="3"/><font color="red"><b> * Harus Huruf Maks 3 Huruf</b></font></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<button type="submit" name="btnproses" value="simpan_alternatif" class="btn btn-primary" data-rel="tooltip" title="Klik Untuk Simpan Alternatif Baru"><i class="icon-ok icon-white"></i> <b>Simpan Data Alternatif</b></button>
				<button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i> <b>Batal</b></button>
			</td>
		</tr>
	</table>
</form>
<form name="myform" method="post" action="proses.php">
	<table width="60%" class="table table-bordered">
		<tbody>
			<tr bgcolor='Powderblue'>
				<th>Pilih</th>
				<th>Nama Alternatif</th>
				<th>Nama Alias</th>
			</tr>
			<?php
				include"../db/koneksi.php";
				$sql=mysql_query("SELECT * FROM tblalternatif");
				$no=0;
				while($data=mysql_fetch_array($sql))
				{
					$no++;
					
			?>
			<tr bgcolor="#FFF">
				<td align="center"><input type="checkbox" name="id_a[]" value="<?php echo $data['idalternatif']?>"></td>
				<td>
					<input type="hidden" name="idalternatif[]" value="<?php echo $data['idalternatif']?>">
					<input class="isi" type="text" name="nama_a[]" value="<?php echo $data['nama_alternatif']?>" style="border:none;width:500px;background-color:none">
				</td>
				<td><?php echo $data['nama_alias']?></td>
			</tr>
			<?php
				}
			?>
		</tbody>
	</table>
	<div>
		<input type="hidden" name="txtjumlah" value="<?php echo $no?>">
		<button type="submit" name="btnproses" value="simpan_perubahan_alternatif" class="btn btn-success" data-rel="tooltip" title="Klik Untuk Simpan Perubahan Alternatif"><i class="icon-share icon-white"></i> <b>Simpan Perubahan Alternatif</b></button>
		<button type="submit" name="btnproses" value="hapus_alternatif" class="btn btn-danger" title="Klik Untuk Hapus Data Alternatif" onClick="return confirm('Apakah Data Alternatif ini Akan Dihapus ??');"><i class="icon-remove icon-white"></i> <b>Hapus Alternatif</b></button>
	</div>
</form>
<?php
	}
	else
	{
		include"error_page.php";
	}
?>