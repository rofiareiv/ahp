﻿<?php
	include"../db/koneksi.php";
	session_start();
	if(!empty($_SESSION['iduser']) && !empty($_SESSION['username']) && !empty($_SESSION['password']))
	{
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>:: SPK Pemilihan TOKO Menggunakan AHP ::</title>
	<!-- The styles -->
	
	<link id="bs-css" href="css/bootstrap-cerulean.css" rel="stylesheet">   
    <style type="text/css">
            @import "media/css/demo_table_jui.css";
            @import "media/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>
	<style type="text/css">
	  body 
	  {
		padding-bottom: 40px;
	  }
	  .sidebar-nav 
	  {
		padding: 9px 0;
	  }
	</style>
	<link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/novi-app.css" rel="stylesheet">
	<link href="css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href='css/fullcalendar.css' rel='stylesheet'>
	<link href='css/fullcalendar.print.css' rel='stylesheet'  media='print'>
	<link href='css/chosen.css' rel='stylesheet'>
	<link href='css/uniform.default.css' rel='stylesheet'>
	<link href='css/colorbox.css' rel='stylesheet'>
	<link href='css/jquery.cleditor.css' rel='stylesheet'>
	<link href='css/jquery.noty.css' rel='stylesheet'>
	<link href='css/noty_theme_default.css' rel='stylesheet'>
	<link href='css/elfinder.min.css' rel='stylesheet'>
	<link href='css/elfinder.theme.css' rel='stylesheet'>
	<link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
	<link href='css/opa-icons.css' rel='stylesheet'>
	<link href='css/uploadify.css' rel='stylesheet'>

	<!-- The fav icon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>
	<script src="media/js/jquery.js" type="text/javascript"></script>
    <script src="media/js/jquery.dataTables.js" type="text/javascript"></script>
	<!--
	<script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $('#datatables').dataTable({
                    "sPaginationType":"full_numbers",
                    "aaSorting":[[2, "desc"]],
                    "bJQueryUI":true
                });
            })
            
    </script>
	-->
	<script>
		function tb8_makeArray(n){
		this.length = n;
		return this.length;
		}

		tb8_messages = new tb8_makeArray(3);
		tb8_messages[0] = "Aplikasi Sistem Pendukung Keputusan ";
		tb8_messages[1] = "Pemilihan TOKO ";
		tb8_messages[2] = "dengan Metode AHP";
		tb8_rptType = 'infinite'
		tb8_rptNbr = 5;
		tb8_speed = 100;
		tb8_delay = 2000;
		var tb8_counter=1;
		var tb8_currMsg=0;
		var tb8_tekst ="";
		var tb8_i=0;
		var tb8_TID = null;
		function tb8_pisi(){
		tb8_tekst = tb8_tekst + tb8_messages[tb8_currMsg].substring(tb8_i, tb8_i+1);
		document.title = tb8_tekst;
		tb8_sp=tb8_speed;
		tb8_i++;
		if (tb8_i==tb8_messages[tb8_currMsg].length){
		tb8_currMsg++; tb8_i=0; tb8_tekst="";tb8_sp=tb8_delay;
		}
		if (tb8_currMsg == tb8_messages.length){
		if ((tb8_rptType == 'finite') && (tb8_counter==tb8_rptNbr)){
		clearTimeout(tb8_TID);
		return; } tb8_counter++;
		tb8_currMsg = 0;
		}
		tb8_TID = setTimeout("tb8_pisi()", tb8_sp);
		}
		tb8_pisi()
		
		//fungsi enter
		function nextfield(ID)
		{
			if (event.keyCode == 13)
			document.getElementById(ID).focus();
		}
		//fungsi open window
		function openwindow(x)
		{
			var hal = x;
			window.open(hal,'new','width=518,height=450,menubars=yes,scrollbars=no');
		}
	</script>
	<script type="text/javascript">
		function pilihan()
		{
			// membaca jumlah komponen dalam form bernama 'myform'
			var jumKomponen = document.myform.length;
			// jika checkbox 'Pilih Semua' dipilih
			if (document.myform[0].checked == true)
			{
				// semua checkbox pada data akan terpilih
				for (i=1; i<=jumKomponen; i++)
				{
					if (document.myform[i].type == "checkbox") document.myform[i].checked = true;
				}
			}
			 // jika checkbox 'Pilih Semua' tidak dipilih
			else if (document.myform[0].checked == false)
			{
				// semua checkbox pada data tidak dipilih
				for (i=1; i<=jumKomponen; i++)
				{
				   if (document.myform[i].type == "checkbox") document.myform[i].checked = false;
				}
			}
		}
	</script>
</head>
<body>
		<!-- topbar starts -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
			
				
				<!-- theme selector starts -->
				<div class="btn-group pull-right theme-container" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-tint"></i><span class="hidden-phone"> Change Theme / Skin</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" id="themes">
						<li><a data-value="classic" href="#"><i class="icon-th-large"></i> Classic</a></li>
						<li><a data-value="cerulean" href="#"><i class="icon-check"></i> Cerulean</a></li>
						<li><a data-value="cyborg" href="#"><i class="icon-star"></i> Cyborg</a></li>-->
						<li><a data-value="redy" href="#"><i class="icon-move"></i> Redy</a></li>
						<li><a data-value="journal" href="#"><i class="icon-leaf"></i> Journal</a></li>-->
						<li><a data-value="simplex" href="#"><i class="icon-gift"></i> Simplex</a></li>-->
						<li><a data-value="slate" href="#"><i class="icon-star-empty"></i> Slate</a></li>
						<li><a data-value="spacelab" href="#"><i class="icon-cog"></i> Spacelab</a></li>
						<li><a data-value="united" href="#"><i class="icon-eye-open"></i> United</a></li>
					</ul>
				</div>
				<!-- theme selector ends -->
				
				<!-- user dropdown starts -->
				<div class="btn-group pull-right" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user"></i><span class="hidden-phone"> Admin</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="home.php?module=<?php echo md5("profile")?>"><i class="icon-user"></i> Profile</a></li>
						<li class="divider"></li>
						<li><a href="logout.php" onClick="return confirm('Yakin Anda Akan Keluar Dari Aplikasi Ini ??');"><i class="icon-off"></i> Logout</a></li>
					</ul>
				</div>
				<!-- user dropdown ends -->
				
			  <div class="top-nav nav-collapse">
						<table width="70%" border="0">
							<tr>
								<td width="95px" align="center"><img src="images/uim.png" height="75" width="80"></td>
								<td><font style="font-size:20px;font-family:Tahoma;color:yellow">APLIKASI SISTEM PENDUKUNG KEPUTUSAN PEMILIHAN TOKO MENGGUNAKAN METODE AHP</font><br> <font style="font-family:Tahoma;color:orange;font-size:14px;"><b>Fakultas Teknik Universitas Islam Madura</b></font></td>
							</tr>
						</table>
						<!--<img src="img/AA-1.png" />-->
			</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav" style="height:400px">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet"><b>Menu ADMIN</b></li>
						<li><a class="ajax-link" href="home.php?module=<?php echo md5("home")?>"><i class="icon-home"></i><span class="hidden-tablet" data-rel="tooltip" title="Klik untuk Ke Halaman Utama">&nbsp;&nbsp;<b>Halaman Utama</b></span></a></li>
						<li><a class="ajax-link" href="home.php?module=<?php echo md5("kriteria")?>"><i class="icon-book"></i><span class="hidden-tablet" data-rel="tooltip" title="Klik untuk Management Data Kriteria">&nbsp;&nbsp;<b>Data Kriteria</b></span></a></li>
						<li><a class="ajax-link" href="home.php?module=<?php echo md5("alternatif")?>"><i class="icon-folder-open"></i><span class="hidden-tablet" data-rel="tooltip" title="Klik untuk Management Data Alternatif">&nbsp;&nbsp;<b>Data Alternatif</b></span></a></li>
						<li><a class="ajax-link" href="home.php?module=<?php echo md5("bobotalternatif")?>"><i class="icon-edit"></i><span class="hidden-tablet" data-rel="tooltip" title="Klik untuk Melakukan Pembobotan Pada Data Alternatif">&nbsp;&nbsp;<b>Pemobotan Alternatif</b></span></a></li>
						<li><a class="ajax-link" href="proses-ahp.php"><i class="icon-refresh"></i><span class="hidden-tablet" data-rel="tooltip" title="Klik untuk Proses Pengambilan Keputusan dengan Metode AHP">&nbsp;&nbsp;<b>Proses AHP</b></span></a></li>
						<li><a class="ajax-link" href="home.php?module=<?php echo md5("keputusan")?>"><i class="icon-list"></i><span class="hidden-tablet" data-rel="tooltip" title="Klik untuk Melihat Hasil Pengambilan Keputusan">&nbsp;&nbsp;<b>Keputusan</b></span></a></li>
						<li><a class="ajax-link" href="home.php?module=<?php echo md5("setting")?>"><i class="icon-wrench"></i><span class="hidden-tablet" data-rel="tooltip" title="Klik untuk Melakukan Perubahan Data Account">&nbsp;&nbsp;<b>Pengaturan</b></span></a></li>
						<li><a class="ajax-link" href="home.php?module=<?php echo md5("user")?>"><i class="icon-user"></i><span class="hidden-tablet" data-rel="tooltip" title="Klik untuk Menambah Pengguna Baru">&nbsp;&nbsp;<b>Tambah Pengguna</b></span></a></li>
						<li><a class="ajax-link" href="logout.php" onClick="return confirm('Yakin Anda Akan Keluar dari Aplikasi Ini ??');"><i class="icon-off"></i><span class="hidden-tablet" data-rel="tooltip" title="Klik untuk Keluar dari Aplikasi">&nbsp;&nbsp;<b>Keluar</b></span></a></li>
					</ul>
				</div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">&nbsp;</h4>
			</div>
			</noscript>
			
			<div id="content" class="span10">
				<div style="width:1200px">
				<ul class="breadcrumb">
					<li>
						<?php
							date_default_timezone_set('Asia/Jakarta'); // PHP 6 mengharuskan penyebutan timezone.
							$seminggu = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
							$hari = date("w");
							$hari_ini = $seminggu[$hari];
							$namabulan="";
							$tgl_sekarang = date("Ymd");
							$tgl  = date("d");
							$bln  = date("m");
							$thn  = date("Y");
							//$jam_sekarang = date("H:i");
							switch ($bln)
							{
								case 1: 
									$namabulan = "Januari";
									break;
								case 2:
									$namabulan = "Februari";
									break;
								case 3:
									$namabulan = "Maret";
									break;
								case 4:
									$namabulan = "April";
									break;
								case 5:
									$namabulan = "Mei";
									break;
								case 6:
									$namabulan = "Juni";
									break;
								case 7:
									$namabulan = "Juli";
									break;
								case 8:
									$namabulan = "Agustus";
									break;
								case 9:
									$namabulan = "September";
									break;
								case 10:
									$namabulan = "Oktober";
									break;
								case 11:
									$namabulan = "November";
									break;
								case 12:
									$namabulan = "Desember";
									break;
							}
							$sql=mysql_query("SELECT * FROM tbluser Where iduser=".$_SESSION['iduser']);
							if($db = mysql_fetch_array($sql))
							{
								$dbid = strtoupper($db['iduser']);
								$dbusername = $db['username'];
								$dbpassword = $db['password'];
						?>
						<b style="color:#090">User Aktif : <?php echo strtoupper($dbusername)?>&nbsp;&nbsp; Hari : <?php echo $hari_ini.", ".$tgl." ".$namabulan." ".$thn?> | Jam : <?php include "jam.php"?></b>
						<?php
							}
						?>
					</li>
					<li>&nbsp;</li>
					<!--
					<li>
						<span style="font-size:16px;font-family:Calibri;color:#F33;padding:10px;margin-right:150px;margin-top:10px"><b><marquee direction="right" scrolldelay="100">Aplikasi Sistem Pendukung Keputusan Pemilihan TOKO Menggunakan Metode AHP</marquee></b><span>
					</li>
					-->
				</ul>
				</div>
				<!-- content starts -->
				<div class="sortable row-fluid"></div>
				<div class="row-fluid">
					<div class="box span12" style="width:1200px">
						<div class="box-header well">
							<h2><i class="icon-info-sign"></i> Content</h2>
							<div class="box-icon">
								<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
								<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
								<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
							</div>
						</div>
						<div class="box-content">
						<!---------area content------>
						<?php
							$module = $_GET['module'];
							if(empty($module)||$module==""||$module==null || $module==md5("home"))
							{
						?>
								<p style="font-size:23px;color:red">Selamat Datang Admin <b><?php echo strtoupper($_SESSION['username'])?></b> Di Aplikasi SPK Pemilihan TOKO  Menggunakan Metode AHP</p>
								<p>&nbsp;</p>
								<p style="font-size:19px;font-family:Candara,Calibri" align="justify">Assalamu'alaikum.Wr.Wb</p>
								<p style="font-size:19px;font-family:Candara,Calibri" align="justify">Alhamdulillah saya bersyukur kepada Allah SWT yang telah memberikan rahmatnya kepada saya sehingga saya bisa menyelesaikan Tugas Akhir ini dengan Baik dan Tepat Waktu. Sholawat dan salam semoga tetap tercurah limpahkan keharibaan junjungan nabi kita Muhammad SAW yang dengan jasa beliaulah kita bisa membedakan yang benar dan yang salah.</p>
								<p>&nbsp;</p>
								<p>&nbsp;</p>
								<p style="font-size:20px;font-family:Calibri"><b><u>ADMINISTRATOR</u></b></p>
						<?php
							}
							else if($module==md5("kriteria"))
								include "fkriteria.php";
							else if($module==md5("alternatif"))
								include "falternatif.php";
							else if($module==md5("bobotalternatif"))
								include "fbobotalternatif.php";
							else if($module==md5("keputusan"))
								include "keputusan.php";
							else if($module==md5("setting"))
								include "fsetting.php";
							else if($module==md5("user"))
								include "fuser.php";
							else if($module==md5("profile"))
							{
						?>
							<font size="4" face="Cambria,Calibri">
							<p align="justify">Nama Program : Aplikasi Sistem Pendukung Keputusan Pemilihan TOKO Menggunakan Metode AHP</p>
							<p>Versi Program : Versi 1.0</p>
							<p>Nama User : <?php echo $_SESSION['username']?></p>
							<p>Session ID : <?php echo session_id()?></p>
							<p>Browser yang Digunakan : <script>document.write(navigator.appName)</script></p>
							<p>Versi Browser : <script>document.write(navigator.appVersion)</script></p>
							<p>Platform : <script>document.write(navigator.platform)</script></p>
							<p align="justify">Aplikasi ini dibuat untuk membantu pengelola toko dalam menentukan Toko terbaik untuk mensuplai barang. sehingga penilaian bisa dilakukan seakurat mungkin.</p>
							</font>
						<?php
							}
						?>
						<div class="clearfix"></div>
						</div>
					</div>
				</div>					
			</div>
			<!-- content ends -->
		</div><!--/#content.span10-->
		</div><!--/fluid-row-->	
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings to be continue...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
		<footer>
			<br><br>
			<p class="pull-left">Copyright &copy; 2016 Universitas Islam Madura</p>
			<p class="pull-right">Powered By : Fakultas Teknik</p>
		</footer>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- jQuery -->
	<script src="js/jquery-1.7.2.min.js"></script>
	<!-- jQuery UI -->
	<script src="js/jquery-ui-1.8.21.custom.min.js"></script>
	<!-- transition / effect library -->
	<script src="js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script src="js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script src="js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script src="js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script src="js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script src="js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script src="js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script src="js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script src="js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src="js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src="js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script src="js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script src="js/bootstrap-tour.js"></script>
	<!-- library for cookie management -->
	<script src="js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script src='js/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='js/jquery.dataTables.min.js'></script>

	<!-- chart libraries start -->
	<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.min.js"></script>
	<script src="js/jquery.flot.pie.min.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script src="js/jquery.chosen.min.js"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="js/jquery.uniform.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="js/jquery.colorbox.min.js"></script>
	<!-- rich text editor library -->
	<script src="js/jquery.cleditor.min.js"></script>
	<!-- notification plugin -->
	<script src="js/jquery.noty.js"></script>
	<!-- file manager library -->
	<script src="js/jquery.elfinder.min.js"></script>
	<!-- star rating plugin -->
	<script src="js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="js/charisma.js"></script>	
	<script src="..js/jquery-ui-1.9.0.js"></script>
	<script>
		// memformat angka ribuan
		function formatAngka(angka) {
		 if (typeof(angka) != 'string') angka = angka.toString();
		 var reg = new RegExp('([0-9]+)([0-9]{3})');
		 while(reg.test(angka)) angka = angka.replace(reg, '$1.$2');
		 return angka;
		}
		// tambah event keypress untuk input bayar
		$('#diskon').on('keypress', function(e) {
		 var c = e.keyCode || e.charCode;
		 switch (c) {
		  case 8: case 9: case 27: case 13: return;
		  case 65:
		   if (e.ctrlKey === true) return;
		 }
		 if (c < 48 || c > 57) e.preventDefault();
		}).on('keyup', function() {
		var inp = $(this).val().replace(/\./g, '');
		  
		// set nilai ke variabel bayar
		diskon = new Number(inp);
		$(this).val(formatAngka(inp));
		
		$('#diskon').val(formatAngka(diskon));
		});
	</script>
</body>
</html>
<?php
	}
	else
		echo "<script>alert('Maaf Anda Harus Login Untuk Dapat Melakukan Proses AHP..!');location.href=\"../index.php\";</script>";
?>
