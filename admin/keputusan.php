<?php
	if(!empty($_SESSION['iduser']) && !empty($_SESSION['username']) && !empty($_SESSION['password']))
	{
		include"../db/koneksi.php";
?>
<h3>Data Bobot Kriteria</h3>
<table class="table table-bordered">
	<tr>
		<td>X</td>
		<?php
			$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY simbol ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td><?php echo $data_simbol['simbol']?></td>
		<?php
			}
		?>
	</tr>
	<?php
		$sql = mysql_query("SELECT * FROM tblbobotkriteria ORDER BY simbol ASC");
		$ke=0;
		while($data = mysql_fetch_array($sql))
		{
			$ke++;
	?>
	<tr>
		<td><?php echo $data['simbol']?></td>
		<?php
			$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td><?php echo $data[$data_simbol['simbol']]?></td>
		<?php
			}
		?>
	</tr>
	<?php
		}
	?>
</table>
<h3>Data Normalisasi Bobot Kriteria</h3>
<table class="table table-bordered">
	<tr>
		<td>X</td>
		<?php
			$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td><?php echo $data_simbol['simbol']?></td>
		<?php
			}
		?>
		<td>Rata-Rata</td>
	</tr>
	<?php
		$sql = mysql_query("SELECT * FROM tblnormalisasikriteria ORDER BY idkriteria ASC");
		$ke=0;
		while($data = mysql_fetch_array($sql))
		{
			$ke++;
	?>
	<tr>
		<td><?php echo $data['simbol']?></td>
		<?php
			$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td><?php echo $data[$data_simbol['simbol']]?></td>
		<?php
			}
		?>
		<td><?php echo $data['rata2']?></td>
	</tr>
	<?php
		}
	?>
	<tr>
		<td colspan="<?php echo $ke+1?>"><b style="color:red">Jumlah Rata-Rata Bobot</b></td>
		<td>
			<?php
				$sql_simbol = mysql_query("SELECT sum(rata2) as jumlah FROM tblnormalisasikriteria");
				if($data_simbol = mysql_fetch_array($sql_simbol))
				{
					echo "<b style=color:red>".round($data_simbol['jumlah'],2).".00</b>";
				}
			?>
		</td>
	</tr>
</table>
<br>
<!--Data Alternatif-->
<?php
	$sql_jumlah = mysql_query("SELECT COUNT(idkriteria) as jumlah FROM tblkriteria");
	$data_jumlah = mysql_fetch_array($sql_jumlah);
	$jumlah_k = $data_jumlah['jumlah'];
	for($tanda=1; $tanda<=$jumlah_k; $tanda++)
	{
?>
<h3>Nilai Bobot Perbandingan Alternatif dengan Kriteria Ke-<?php echo $tanda?></h3>
<table style="border-collapse:collapse;border:1px solid" border="1" width="100%" cellpadding="5">
	<tr>
		<td>X</td>
		<?php
			$sql_simbol = mysql_query("SELECT nama_alias FROM tblalternatif ORDER BY idalternatif ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td><?php echo $data_simbol['nama_alias']?></td>
		<?php
			}
		?>
	</tr>
	<?php
		$sql = mysql_query("SELECT * FROM tblbobotalternatif WHERE tanda='$tanda' ORDER BY idalternatif ASC");
		$ke=0;
		while($data = mysql_fetch_array($sql))
		{
			$ke++;
	?>
	<tr>
		<td><?php echo $data['nama_alias']?></td>
		<?php
			$sql_simbol = mysql_query("SELECT nama_alias FROM tblalternatif ORDER BY idAlternatif ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td><?php echo $data[$data_simbol['nama_alias']]?></td>
		<?php
			}
		?>
	</tr>
	<?php
		}
	?>
</table>
<h3>Menghitung Normalisasi (Eigen Value) Bobot Perbadingan Alternatif dengan Kriteria Ke-<?php echo $tanda?></h3>
<table style="border-collapse:collapse;border:1px solid" border="1" width="100%" cellpadding="5">
	<tr>
		<td>X</td>
		<?php
			$sql_simbol = mysql_query("SELECT nama_alias FROM tblalternatif ORDER BY idAlternatif ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td><?php echo $data_simbol['nama_alias']?></td>
		<?php
			}
		?>
		<td>Rata-Rata</td>
	</tr>
	<?php
		$sql = mysql_query("SELECT * FROM tblnormalisasialternatif WHERE tanda='$tanda' ORDER BY idalternatif ASC");
		$ke=0;
		while($data = mysql_fetch_array($sql))
		{
			$ke++;
	?>
	<tr>
		<td><?php echo $data['nama_alias']?></td>
		<?php
			$sql_simbol = mysql_query("SELECT nama_alias FROM tblalternatif ORDER BY idAlternatif ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td><?php echo $data[$data_simbol['nama_alias']]?></td>
		<?php
			}
		?>
		<td><?php echo $data['rata2']?></td>
	</tr>
	<?php
		}
	?>
	<tr>
		<td colspan="<?php echo $ke+1?>"><b style="color:red">Jumlah Rata-Rata Bobot</b></td>
		<td>
			<?php
				$sql_simbol = mysql_query("SELECT sum(rata2) as jumlah FROM tblnormalisasialternatif WHERE tanda='$tanda'");
				if($data_simbol = mysql_fetch_array($sql_simbol))
				{
					echo "<b style=color:red>".round($data_simbol['jumlah'],2).".00</b>";
				}
			?>
		</td>
	</tr>
</table>
<br><br>
<?php
	}
?>
<h3>Matriks Normalisasi (Eigen Value) Kriteria</h3>
<table style="border-collapse:collapse;border:1px solid" border="1" width="30%" cellpadding="5">
	<th>No</th>
	<th>Simbol</th>
	<th>Nilai</th>
	<?php
		$sql_rata = mysql_query("SELECT * FROM tblnormalisasikriteria ORDER BY idkriteria ASC");
		$no=0;
		while($data = mysql_fetch_array($sql_rata))
		{
			$no++;
	?>
	<tr>
		<td align="center"><?php echo $no?></td>
		<td align="center"><?php echo $data['simbol']?></td>
		<td align="center"><?php echo $data['rata2']?></td>
	</tr>
	<?php
		}
	?>
</table>
<br>
<h3>Matriks Normalisasi (Eigen Value) Alternatif</h3>
<table style="border-collapse:collapse;border:1px solid" border="1" width="70%" cellpadding="5">
	<th>No</th>
	<th>Nama Alternatif</th>
	<?php
		$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
		while($data_simbol = mysql_fetch_array($sql_simbol))
		{
	?>
	<th><?php echo $data_simbol['simbol']?></th>
	<?php
		}
	?>
	</tr>
	<?php
		$sql_rata = mysql_query("SELECT * FROM tblrataalternatif ORDER BY idalternatif ASC");
		$no=0;
		while($data = mysql_fetch_array($sql_rata))
		{
			$no++;
	?>
	<tr>
		<td align="center"><?php echo $no?></td>
		<td align="center"><?php echo $data['nama_alternatif']?></td>
		<?php
			$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td align="center"><?php echo $data[$data_simbol['simbol']]?></td>
		<?php
			}
		?>
	</tr>
	<?php
		}
	?>
</table>
<br>
<h3>Hasil Keputusan (Eigen Vector)</h3>
<table style="border-collapse:collapse;border:1px solid" border="1" width="40%" cellpadding="5">
	<th>No</th>
	<th>Nama Alternatif</th>
	<th>Nilai</th>
	<?php
		$sql_rata = mysql_query("SELECT * FROM tblkeputusan ORDER BY nilai DESC");
		$no=0;
		while($data = mysql_fetch_array($sql_rata))
		{
			$no++;
	?>
	<tr>
		<td align="center"><?php echo $no?></td>
		<td><?php echo $data['nama_alternatif']?></td>
		<td align="center"><?php echo $data['nilai']?></td>
	</tr>
	<?php
		}
	?>
</table>
<br/>
<p style="color:red;font-weight:bold;font-size:20px">Keterangan : Kandidat yang memperolah nilai terbesar menjadi alternatif terbaik</p>
<div>
	<a href="" onclick="window.open('cetak-keputusan.php','new','width=950,height=650,scrollbars=no,menubars=no');"><button type="button" class="btn btn-primary" data-rel="tooltip" title="Klik Untuk Melihat Detail Bobot Alternatif"><i class="icon-print icon-white"></i> <b>Cetak Keputusan</b></button>
</div>
<?php
}
else
	echo "<script>alert('Maaf Anda Harus Login Untuk Dapat Melakukan Proses AHP..!');location.href=\"../index.php\";</script>";
?>
