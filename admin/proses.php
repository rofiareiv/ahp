<?php
	include"../db/koneksi.php";
	$tombol = $_POST['btnproses'];
	if($tombol=="simpan_kriteria")
	{
		$nama_kriteria = $_POST['nama_kriteria'];
		//dapatkan jumlah terakhir kriteria
		$sql_jumlah = mysql_query("SELECT COUNT(idkriteria) AS jumlah FROM tblkriteria");
		$data_jumlah = mysql_fetch_array($sql_jumlah);
		$jumlah = $data_jumlah['jumlah'];
		$simbol = "A".($jumlah+1);
		//cek duplikasi
		$cek = mysql_query("SELECT * FROM tblkriteria WHERE nama_kriteria='$nama_kriteria'");
		if($ada = mysql_fetch_array($cek))
		{
			echo"<script>alert('Nama Kriteria Duplikat,Silahkan Ganti...!');location.href=\"home.php?module=".md5("kriteria")."\";</script>";
		}
		else
		{
			//simpan kriteria
			$simpan_kriteria = mysql_query("INSERT INTO tblkriteria VALUES(NULL,'$nama_kriteria','$simbol')");
			//hancurkan dulu tblbobotkriteria dan tblnormalisasikriteria
			$hancurkan = mysql_query("DROP TABLE  tblbobotkriteria");
			$hancurkan = mysql_query("DROP TABLE  tblnormalisasikriteria");
			$hancurkan = mysql_query("DROP TABLE  tblrataalternatif");
			//kemudian buat ulang tabelnya
			$query1="CREATE TABLE IF NOT EXISTS tblbobotkriteria (idbobot int(30) NOT NULL AUTO_INCREMENT,idkriteria int(30) NOT NULL,simbol varchar(20) NOT NULL,";
			$query2="CREATE TABLE IF NOT EXISTS tblnormalisasikriteria (idnormalisasi int(30) NOT NULL AUTO_INCREMENT,idkriteria int(30) NOT NULL,simbol varchar(20) NOT NULL,";
			$query3="CREATE TABLE IF NOT EXISTS tblrataalternatif (idrata int(30) NOT NULL AUTO_INCREMENT,idalternatif int(30) NOT NULL,nama_alternatif varchar(20) NOT NULL,";
			$sql_kriteria = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
			$ke=0;
			while($data_alias = mysql_fetch_array($sql_kriteria))
			{
				$query1.=$data_alias['simbol']." double NOT NULL,";
				$query2.=$data_alias['simbol']." double NOT NULL,";
				$query3.=$data_alias['simbol']." double NOT NULL,";
			}
			$query1.="PRIMARY KEY (idbobot)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
			$query2.="rata2 DOUBLE NOT NULL, PRIMARY KEY (idnormalisasi)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
			$query3.="PRIMARY KEY (idrata)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
			$buat_ulang  = mysql_query($query1);	
			$buat_ulang  = mysql_query($query2);
			$buat_ulang  = mysql_query($query3);
			echo"<script>alert('Kriteria Sudah Tersimpan....!');location.href=\"home.php?module=".md5("kriteria")."\";</script>";
			
		}
	}
	else if($tombol=="simpan_bobot_kriteria")
	{
		//kosongkan dulu
		$kosongkan = mysql_query("TRUNCATE TABLE tblbobotkriteria");
		$kosongkan = mysql_query("TRUNCATE TABLE tblnormalisasikriteria");
		$jumlah = $_POST['jumlah'];
		$id_k = $_POST['id_k'];
		$simbol_k = $_POST['simbol_k'];
		for($ke=1; $ke<=$jumlah; $ke++)
		{
			$data="";
			$total=0;
			$rata2=0;
			$idkriteria = $id_k[$ke-1];
			$simbol = $simbol_k[$ke-1];
			for($k=0; $k<$jumlah; $k++)
			{
				$nilai = $_POST[$simbol_k[$k].$ke];
				$total+=$nilai;
				if($k<$jumlah-1)
					$data.="'".$nilai."',";
				else
					$data.="'".$nilai."'";
			}
			$rata2 = round(($total/$jumlah),3);
			//simpan data bobot kriteria
			$simpan_bobot = mysql_query("INSERT INTO tblbobotkriteria VALUES(NULL,'$idkriteria','$simbol',$data)");
		}
		//simpan satu data
		$nol="";
		for($k=0; $k<$jumlah; $k++)
		{
			if($k<$jumlah-1)
				$nol.="'0',";
			else
				$nol.="'0'";
		}
		$id = $jumlah+1;
		//echo $nol;
		//buat satu baris data lagi
		$simpan_bobot = mysql_query("INSERT INTO tblbobotkriteria VALUES(NULL,'$id','jumlah',$nol)");
		//hitung nilai totalnya kriteria
		//echo $jumlah;
		for($i=0; $i<$jumlah; $i++)
		{
			$simbol = $simbol_k[$i];
			//hitung jumlah per kolom
			$sql = mysql_query("SELECT SUM($simbol) AS jumlah FROM tblbobotkriteria");
			$data = mysql_fetch_array($sql);
			$jumlahnya = round($data['jumlah'],3);
			//update nilai rata2
			$update =  mysql_query("UPDATE tblbobotkriteria SET $simbol='$jumlahnya' WHERE simbol='jumlah'");
		}
		//melakukan normalisasi pada bobot kriteria
		for($ke=0; $ke<$jumlah; $ke++)
		{
			$data_normalisasi="";
			$total=0;
			$rata2=0;
			$idkriteria = $id_k[$ke];
			$simbol = $simbol_k[$ke];
			for($k=0; $k<$jumlah; $k++)
			{
				$simbol2 = $simbol_k[$k];
				//ambil nilai perbaris kemudian dibagi dengan jumlah
				$sql_ambil = mysql_query("SELECT $simbol2 FROM tblbobotkriteria WHERE idkriteria='$idkriteria' AND simbol='$simbol'");
				$data_nilai = mysql_fetch_array($sql_ambil);
				$bobot_k = $data_nilai[$simbol2];
				//ambil jumlah kolom berdasarkan simbol
				$sql_jumlah = mysql_query("SELECT $simbol2 FROM tblbobotkriteria WHERE simbol='jumlah'");
				$data_jumlah = mysql_fetch_array($sql_jumlah);
				$jum_bobot = $data_jumlah[$simbol2];
				$hasil = round(($bobot_k/$jum_bobot),3);
				$total+=$hasil;
				if($k<$jumlah-1)
					$data_normalisasi.="'".$hasil."',";
				else
					$data_normalisasi.="'".$hasil."'";
			}
			//simpan hasil normalisasi
			$simpan_normalisasi = mysql_query("INSERT INTO tblnormalisasikriteria VALUES(NULL,'$idkriteria','$simbol',$data_normalisasi,0)");
			//update nilai rata-rata
			$rata2 = round(($total/$jumlah),3);
			$update = mysql_query("UPDATE tblnormalisasikriteria SET rata2='$rata2' WHERE idkriteria='$idkriteria' ANd simbol='$simbol'");
		}
		echo"<script>alert('Bobot Kriteria Sudah Disimpan..!');location.href=\"home.php?module=".md5("kriteria")."\";</script>";
	}
	else if($tombol=="hapus_kriteria")
	{
		if(empty($_POST['id_k']))
		{
			echo"<script>alert('Pilih Kriteria yang Akan Dihapus..!');location.href=\"home.php?module=".md5("kriteria")."\";</script>";
		}
		else
		{
			$id_k=$_POST['id_k'];
			foreach($id_k as $idnya)
			{
				//ambil simbol dari kriteria yang dihapus
				$sql = mysql_query("DELETE FROM tblkriteria WHERE idkriteria='$idnya'");
			}
			//dapatkan jumlah terakhir kriteria
			$sql_datakriteria = mysql_query("SELECT * FROM tblkriteria ORDER BY idkriteria ASC");
			$simbolke=0;
			while($datak = mysql_fetch_array($sql_datakriteria))
			{
				$simbolke++;
				$idkriteria = $datak['idkriteria'];
				$update_simbol = mysql_query("UPDATE tblkriteria SET simbol='A$simbolke' WHERE idkriteria='$idkriteria'");
			}
			//hancurkan dulu tblbobotkriteria dan tblnormalisasikriteria
			$hancurkan = mysql_query("DROP TABLE  tblbobotkriteria");
			$hancurkan = mysql_query("DROP TABLE  tblnormalisasikriteria");
			$hancurkan = mysql_query("DROP TABLE  tblrataalternatif");
			//kemudian buat ulang tabelnya
			$query1="CREATE TABLE IF NOT EXISTS tblbobotkriteria (idbobot int(30) NOT NULL AUTO_INCREMENT,idkriteria int(30) NOT NULL,simbol varchar(20) NOT NULL,";
			$query2="CREATE TABLE IF NOT EXISTS tblnormalisasikriteria (idnormalisasi int(30) NOT NULL AUTO_INCREMENT,idkriteria int(30) NOT NULL,simbol varchar(20) NOT NULL,";
			$query3="CREATE TABLE IF NOT EXISTS tblrataalternatif (idrata int(30) NOT NULL AUTO_INCREMENT,idalternatif int(30) NOT NULL,nama_alternatif varchar(20) NOT NULL,";
			$sql_kriteria = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
			$ke=0;
			while($data_alias = mysql_fetch_array($sql_kriteria))
			{
				$query1.=$data_alias['simbol']." double NOT NULL,";
				$query2.=$data_alias['simbol']." double NOT NULL,";
				$query3.=$data_alias['simbol']." double NOT NULL,";
			}
			$query1.="PRIMARY KEY (idbobot)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
			$query2.="rata2 DOUBLE NOT NULL, PRIMARY KEY (idnormalisasi)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
			$query3.="PRIMARY KEY (idbobot)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
			$buat_ulang  = mysql_query($query1);	
			$buat_ulang  = mysql_query($query2);	
			$buat_ulang  = mysql_query($query3);	
			echo"<script>alert('Data Kriteria yang Dipilih Sudah Dihapus..!');location.href=\"home.php?module=".md5("kriteria")."\";</script>";
		}
	}
	else if($tombol=="simpan_alternatif")
	{
		$nama_kandidat = addslashes(strtoupper($_POST['nama_kandidat']));
		$nama_alias = strtoupper($_POST['nama_alias']);
		//cek duplikasi alias
		$cek = mysql_query("SELECT nama_alias FROM tblalternatif WHERE nama_alias='$nama_alias'");
		if(!$ada = mysql_fetch_array($cek))
		{
			//simpan alternatif
			$simpan_alternatif = mysql_query("INSERT INTO tblalternatif VALUES(NULL,'$nama_kandidat','$nama_alias')");
			//hancurkan dulu tblbobotalternatif dan tblnormalisasialternatif
			$hancurkan = mysql_query("DROP TABLE tblbobotalternatif");
			$hancurkan = mysql_query("DROP TABLE tblnormalisasialternatif");
			//buat kembali
			$query1="CREATE TABLE IF NOT EXISTS tblbobotalternatif (idbobot int(30) NOT NULL AUTO_INCREMENT,idalternatif int(30) NOT NULL,nama_alias varchar(20) NOT NULL,";
			$query2="CREATE TABLE IF NOT EXISTS tblnormalisasialternatif (idnormalisasi int(30) NOT NULL AUTO_INCREMENT,idalternatif int(30) NOT NULL,nama_alias varchar(20) NOT NULL,";
			$sql_alternatif = mysql_query("SELECT nama_alias FROM tblalternatif ORDER BY idalternatif ASC");
			while($data_alias = mysql_fetch_array($sql_alternatif))
			{
				$query1.=$data_alias['nama_alias']." double NOT NULL,";
				$query2.=$data_alias['nama_alias']." double NOT NULL,";
			}
			$query1.="tanda int(10) NOT NULL, PRIMARY KEY (idbobot)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
			$query2.="rata2 DOUBLE NOT NULL,tanda int(10) NOT NULL, PRIMARY KEY (idnormalisasi)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
			$buat_ulang  = mysql_query($query1);	
			$buat_ulang  = mysql_query($query2);
			echo"<script>alert('Alternatif Baru Sudah Disimpan....!');location.href=\"home.php?module=".md5("alternatif")."\";</script>";
		}
		else
			echo"<script>alert('Nama Alias Alternatif Duplikat, Silahkan Ganti....!');location.href=\"home.php?module=".md5("alternatif")."\";</script>";
	}
	else if($tombol=="simpan_perubahan_alternatif")
	{
		$id_a = $_POST['idalternatif'];
		$nama_a = $_POST['nama_a'];
		$jumlah = $_POST['txtjumlah'];
		for($i=0; $i<$jumlah; $i++)
		{
			$id_alternatif = $id_a[$i];
			$nama_alternatif = addslashes(strtoupper($nama_a[$i]));
			//update data alternatif
			$update_krteria = mysql_query("UPDATE tblalternatif SET nama_alternatif='$nama_alternatif' WHERE idalternatif='$id_alternatif'");
		}
		echo"<script>alert('Perubahan Data Alternatif Berhasil Dilakukan..!');location.href=\"home.php?module=".md5("alternatif")."\";</script>";
	}
	else if($tombol=="hapus_alternatif")
	{
		if(empty($_POST['id_a']))
		{
			echo"<script>alert('Pilih Data Alternatif yang Akan Dihapus..!');location.href=\"home.php?module=".md5("alternatif")."\";</script>";
		}
		else
		{
			$id_a=$_POST['id_a'];
			foreach($id_a as $idnya)
			{
				//hapus data alternatif
				$sql = mysql_query("DELETE FROM tblalternatif WHERE idalternatif='$idnya'");
			}
			//tblbobotalternatif dan tblnormalisasi alternatif disusun ulang
			//hancurkan dulu tblbobotalternatif dan tblnormalisasialternatif
			$hancurkan = mysql_query("DROP TABLE tblbobotalternatif");
			$hancurkan = mysql_query("DROP TABLE tblnormalisasialternatif");
			//buat kembali
			$query1="CREATE TABLE IF NOT EXISTS tblbobotalternatif (idbobot int(30) NOT NULL AUTO_INCREMENT,idalternatif int(30) NOT NULL,nama_alias varchar(20) NOT NULL,";
			$query2="CREATE TABLE IF NOT EXISTS tblnormalisasialternatif (idnormalisasi int(30) NOT NULL AUTO_INCREMENT,idalternatif int(30) NOT NULL,nama_alias varchar(20) NOT NULL,";
			$sql_alternatif = mysql_query("SELECT nama_alias FROM tblalternatif ORDER BY idalternatif ASC");
			while($data_alias = mysql_fetch_array($sql_alternatif))
			{
				$query1.=$data_alias['nama_alias']." double NOT NULL,";
				$query2.=$data_alias['nama_alias']." double NOT NULL,";
			}
			$query1.="tanda int(10) NOT NULL, PRIMARY KEY (idbobot)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
			$query2.="rata2 DOUBLE NOT NULL,tanda int(10) NOT NULL, PRIMARY KEY (idnormalisasi)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
			$buat_ulang  = mysql_query($query1);	
			$buat_ulang  = mysql_query($query2);
			echo"<script>alert('Data Alternatif yang Dipilih Sudah Dihapus..!');location.href=\"home.php?module=".md5("alternatif")."\";</script>";
		}
	}
	else if($tombol=="simpan_bobot_alternatif")
	{
		$no = $_POST['no'];
		$jumlah = $_POST['ke'];
		//hapus data lama
		$kosongkan = mysql_query("DELETE FROM tblbobotalternatif WHERE tanda='$no'");
		$kosongkan = mysql_query("DELETE FROM tblnormalisasialternatif WHERE tanda='$no'");
		$id_a = $_POST['id_a'];
		$alias_a = $_POST['alias_a'];
		for($ke=1; $ke<=$jumlah; $ke++)
		{
			$data="";
			$idalternatif = $id_a[$ke-1];
			$nama_alias = $alias_a[$ke-1];
			for($k=0; $k<$jumlah; $k++)
			{
				//echo $alias_a[$k].$no.($ke);
				$nilai = $_POST[$alias_a[$k].$no.($ke)];
				//$total+=$nilai;
				if($k<$jumlah-1)
					$data.="'".$nilai."',";
				else
					$data.="'".$nilai."'";
			}
			//echo $data."<br>";
			//simpan data bobot kriteria
			$simpan_bobot = mysql_query("INSERT INTO tblbobotalternatif VALUES(NULL,'$idalternatif','$nama_alias',$data,'$no')");
		}
		//tambah satu data
		$nol="";
		for($k=1; $k<=$jumlah; $k++)
		{
			if($k<$jumlah)
				$nol.="'0',";
			else
				$nol.="'0'";
		}
		$id = $jumlah+1;
		//echo $nol;
		//buat satu baris data lagi
		$simpan_bobot = mysql_query("INSERT INTO tblbobotalternatif VALUES(NULL,'$id','jumlah',$nol,'$no')");
		//hitung nilai totalnya kriteria
		//echo $jumlah;
		for($i=0; $i<$jumlah; $i++)
		{
			$nama_alias = $alias_a[$i];
			//hitung jumlah per kolom
			$sql = mysql_query("SELECT SUM($nama_alias) AS jumlah FROM tblbobotalternatif WHERE tanda='$no'");
			$data = mysql_fetch_array($sql);
			$jumlahnya = round($data['jumlah'],3);
			//update nilai rata2
			$update =  mysql_query("UPDATE tblbobotalternatif SET $nama_alias='$jumlahnya' WHERE nama_alias='jumlah' AND tanda='$no'");
		}
		//melakukan normalisasi pada bobot kriteria
		for($ke=0; $ke<$jumlah; $ke++)
		{
			$data_normalisasi="";
			$total=0;
			$rata2=0;
			$idalternatif = $id_a[$ke];
			$nama_alias = $alias_a[$ke];
			for($k=0; $k<$jumlah; $k++)
			{
				$nama_alias2 = $alias_a[$k];
				//ambil nilai perbaris kemudian dibagi dengan jumlah
				$sql_ambil = mysql_query("SELECT $nama_alias2 FROM tblbobotalternatif WHERE idalternatif='$idalternatif' AND nama_alias='$nama_alias' AND tanda='$no'");
				$data_nilai = mysql_fetch_array($sql_ambil);
				$bobot_a = $data_nilai[$nama_alias2];
				//ambil jumlah kolom berdasarkan simbol
				$sql_jumlah = mysql_query("SELECT $nama_alias2 FROM tblbobotalternatif WHERE nama_alias='jumlah' AND tanda='$no'");
				$data_jumlah = mysql_fetch_array($sql_jumlah);
				$jum_bobot = $data_jumlah[$nama_alias2];
				$hasil = round(($bobot_a/$jum_bobot),3);
				$total+=$hasil;
				if($k<$jumlah-1)
					$data_normalisasi.="'".$hasil."',";
				else
					$data_normalisasi.="'".$hasil."'";
			}
			//simpan hasil normalisasi
			$simpan_normalisasi = mysql_query("INSERT INTO tblnormalisasialternatif VALUES(NULL,'$idalternatif','$nama_alias',$data_normalisasi,0,'$no')");
			//update nilai rata-rata
			$rata2 = round(($total/$jumlah),3);
			$update = mysql_query("UPDATE tblnormalisasialternatif SET rata2='$rata2' WHERE idalternatif='$idalternatif' ANd nama_alias='$nama_alias' AND tanda='$no'");
		}
		echo"<script>alert('Bobot Alternatif Sudah Disimpan..!');location.href=\"home.php?module=".md5("bobotalternatif")."\";</script>";
	}
	else if($tombol=="simpan_account")
	{
		session_start();
		$username = $_POST['txtusername'];
		$password = $_POST['txtpassword'];
		$sql = mysql_query("UPDATE tbluser SET password='$password' WHERE iduser=".$_SESSION['iduser']);
		session_destroy();
		echo"<script>alert('Perubahan Data Account Berhasil Dilakukan, Anda Akan Logout Otomatis..!');location.href=\"../index.php\";</script>";
	}
	else if($tombol=="simpan_user")
	{
		$username = $_POST['txtusername'];
		$password = $_POST['txtpassword'];
		$sql = mysql_query("INSERT INTO tbluser VALUES(NULL,'$username','$password')");
		echo"<script>alert('Pengguna Baru Sudah Ditambahkan Kedalam Database..!');location.href=\"home.php?module=".md5("user")."\";</script>";
	}
?>