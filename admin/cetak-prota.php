<html lang="en">
<head>
<title>Cetak Laporan PROTA</title>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
<link href="images/icon.png" rel="shortcut icon" type="image/png">
<style type="text/css" media="screen,print">
*{
	margin:0;
	padding:0;
}
body{
	background-color:#333333;
	font:normal 8pt/100% tahoma,arial,sans-serif;
}
div.page{
	background-color:white;
	color:black;
	min-height:auto;
	margin:1cm auto;
	padding: 0.5cm 1cm 1cm 1cm;
	width:23.5cm;/*untuk lebar halaman*/
}
div.header{
	background-color:white;
	border-bottom:3px solid black;
	font-family:"Times New Roman",serif;	
	padding-bottom:.5cm;
	text-align:center;
	color:black;
	padding-top:20px;
}
div.header2{
	text-align:center;
	font-size:13pt;
	font-weight:bold;
	line-height:120%;
	padding-top:0.2cm;
}
div.header div.h1{
	font-size:15pt;
	font-weight:bold;
	line-height:18pt;
	font-family:Tahoma,Calibri;
}
div.header div.h2{
	font-size:13pt;	
	line-height:18pt;
	font-family:Calibri;	
}
div.header div.h3{
	font-size:13pt;	
	line-height:12pt;	
}
div.content{
	margin-top:.2cm;
}
table.data{	
	border-collapse:collapse;
	width:100%;
	font-family:Cambria,Calibri;
	font-size:14px;
}
th{
	padding:3px;
	border:1px solid black;
}
table.data td{
	padding:4px;
}
span.pilihan{
	border-bottom:1px dotted black;
}
</style>
<style type="text/css" media="print">
body{
	background-color:white;
}
div.page{
	margin:auto;
	min-height:29.5cm;
	margin:auto;
	padding:0 1cm;	
	width:21.5cm;
}
div.header{		
	padding-bottom:.5cm;
}
</style>
</head>
<body>
	<div class="page">
		<div class="header">
			<img style="float:left;margin-top:-20px;margin-right:-40px" src="../images/index.jpeg" alt="Unira" width="90" height="80">
			<!--
			<img style="float: right;margin-top:-12px" src="images/sehat.png" width="60" height="60">		
			-->
			<div class="h1">SEKOLAH MENENGAH PERTAMA (SMP) NEGERI 3</div>			
			<div class="h1">JL.RAYA PLAMPAAN CAMPLONG SAMPANG </div>
		</div>
		<div class="content">
		<?php
			include"../db/koneksi.php";
			$sql = mysqli_query($conn, "SELECT thnaktif FROM tbljam");
			$data = mysqli_fetch_array($sql);
			$thnaktif = $data['thnaktif'];
		?>
		<center><h2>Daftar PROTA SMPN 3 Camplong Tahun <?php echo $thnaktif?></h2></center>
		<br>
		<table width="100%" class="data">
		<tr>
			<td>
					<table border="1" width="100%" class="data">
						<th>No</th>
						<th>Nama Program Tahunan</th>
						<th>Pelaksana</th>
						<th>Tahun</th>
						<th>Status</th>
						<?php
							include"../db/koneksi.php";
							$sql = mysqli_query($conn, "SELECT * FROM tblprota Where thnprota='$thnaktif' ORDER BY status DESC");
							$no=0;
							while($data = mysqli_fetch_array($sql))
							{
								$no++;
								$ok = $data['status'];
								if($ok==0)
									$status="Tidak Terlaksana";
								else
									$status="Terlaksana";
						?>
						<tr>
							<td align="center"><?php echo $no?></td>
							<td><?php echo $data['kegiatan']?></td>
							<td><?php echo $data['pelaksana']?></td>
							<td align="center"><?php echo $data['thnprota']?></td>
							<td><?php echo $status?></td>
						</tr>
						<?php
							}
						?>
					</table>
			</td>
		</tr>
	</table>	
		<br><br>
		<table border="0" width="100%">
			<tr>
				<td>&nbsp;</td>
				<td width="550px">&nbsp;</td>
				<td>Sampang, <?php echo date('d-m-Y')?></td>
			</tr>
			<tr>
				<td></td>
				<td>&nbsp;</td>
				<td>Kepala Sekolah</td>
			</tr>
			<tr height="160px">
				<td></td>
				<td>&nbsp;</td>
				<td><b><u>SIHABUDDIN,M.H</b></u></td>
			</tr>
		</table>
		</div>
</body>