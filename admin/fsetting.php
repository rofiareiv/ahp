<style type="text/css">
	  body 
	  {
		padding-bottom: 40px;
	  }
	  .sidebar-nav 
	  {
		padding: 9px 0;
	  }

	div#form-login {
	width:500px; margin:0 auto;
	margin-top:30px;
	padding:20px; background-color:#FFFFFF;
	border-radius:15px; -moz-border-radius:25px; 
	-webkit-border-radius:15px;border:1px solid #666;
	text-align:left;font-size: small;
	font-family: Cambria,Tahoma,"Lucida Grande","Lucida Sans Unicode", Tahoma, sans-serif;
	letter-spacing: .01em;
	color:#000099
	}
	div#form-login form { 
	margin:0;padding:0;border-radius:15px; -moz-border-radius:25px; 
	-webkit-border-radius:15px;border:1px solid #666;
	background-color:#FFFFFF;
	-webkit-box-shadow: rgba(0,0,0,0.25) 5px 5px 10px;
	-moz-box-shadow: rgba(0,0,0,0.25) 5px 5px 10px;
	box-shadow: rgba(0,0,0,0.25) 5px 5px 10px;
	}
	div#form-login form label { float:right; width:120px;}
	div#form-login form .txtx { padding:3px;margin:3px;}
	div#form-login h1 { font:20px "Trebuchet MS"; border-bottom:1px dotted #009900;padding:5px;margin:10px;}
	div#tombol { clear:both;padding:10px;}
	.button{ box-shadow: rgba(0,0,0,0.1) 0px 1px 1px;padding:3px 20px; }
	.button:hover,.button:focus{
	color: #000; -webkit-box-shadow: rgba(0,0,0,0.25) 1px 1px 3px; 
	-moz-box-shadow: rgba(0,0,0,0.25) 1px 1px 3px; box-shadow: rgba(0,0,0,0.25) 1px 1px 3px;
	}
</style>
<p style="color:#088;font-family:Cambria,Tahoma;font-size:15px;text-align:justify"><b>Untuk Mensetting data account anda, anda bisa melakukannya lewat form dibawah ini. Jika anda sudah melakukan perubahan silahkan disimpan perubahannya dengan klik tombol "Simpan Perubahan Account".</b></p>
<div id="form-login">
<form id="FLogin" name="FLogin" method="post" action="proses.php">
<h1 align="center" style="font-family:Cambria;color:blue">Form Setting Data Account</h1>
	<table border="0" align="center" width="80%">
		<tr>
			<td><label>Username</label></td>
			<td>
				<input name="txtusername" type="text"  style="width:230px;" value="<?php echo $dbusername?>" readonly>
			</td>
		</tr>
		<tr>
			<td><label>Password</label></td>
			<td>
				<input name="txtpassword" type="password"  style="width:230px;" value="<?php echo $dbpassword?>" required="">
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td align="left">	
				<button type="submit" name="btnproses" value="simpan_account" class="btn btn-success" data-rel="tooltip" title="Klik Untuk Menyimpan Perubahan"><li class="icon-user icon-white"></li> Simpan Perubahan Account</button>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
			*) Silahkan Anda Lakukan Perubahan Data Account Secara berkala untuk alasan keamanan
			</td>
		</tr>
	</table>
     <p>&nbsp;</p><br>
</form>
</div>
<br><br>