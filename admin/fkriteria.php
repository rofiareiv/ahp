<style>
	th
	{
		color:#033;
		font-family:Cambria,Verdana;
		font-size:14px;
	}
</style>
<?php
	if(!empty($_SESSION['iduser']) && !empty($_SESSION['username']) && !empty($_SESSION['password']))
	{
?>
<SCRIPT TYPE="text/javascript">
function namesonly(myfield, e, dec)
{
	var key;
	var keychar;

	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which;
	else
		return true;
	keychar = String.fromCharCode(key);
	// control keys
	if ((key==null) || (key==0) || (key==8) ||(key==9) || (key==13) || (key==27) )
		return true;
	// numbers
	else if (((" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'").indexOf(keychar) > -1))
		return true;
	else
		return false;
}
</SCRIPT>
<script>
function pilihan()
	{
     // membaca jumlah komponen dalam form bernama 'myform'
     var jumKomponen = document.myform.length;

     // jika checkbox 'Pilih Semua' dipilih
     if (document.myform[0].checked == true)
     {
        // semua checkbox pada data akan terpilih
        for (i=1; i<=jumKomponen; i++)
        {
            if (document.myform[i].type == "checkbox") document.myform[i].checked = true;
        }
     }
     // jika checkbox 'Pilih Semua' tidak dipilih
     else if (document.myform[0].checked == false)
        {
            // semua checkbox pada data tidak dipilih
            for (i=1; i<=jumKomponen; i++)
            {
               if (document.myform[i].type == "checkbox") document.myform[i].checked = false;
            }
        }
  }
</script>
<form name="fkriteria" method="post" action="proses.php">
	<table cellpadding="5">
		<tr>
			<td><b>Nama Kriteria</b></td>
			<td><b>:</b></td>
			<td><input  type="text" name="nama_kriteria" required="" placeholder="Nama Kriteria..." autofocus style="width:300px" onKeyPress="return namesonly(this, event)"/><font color="red"><b> * Harus Huruf</b></font></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<button type="submit" name="btnproses" value="simpan_kriteria" class="btn btn-primary" data-rel="tooltip" title="Klik Untuk Simpan Kriteria Baru"><i class="icon-ok icon-white"></i> <b>Simpan Data Kriteria</b></button>
				<button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i> <b>Batal</b></button>
			</td>
		</tr>
	</table>
</form>
<form name="myform" method="post" action="proses.php">
	<table width="60%" class="table table-bordered">
		<tbody>
			<tr bgcolor='Powderblue'>
				<th>Pilih</th>
				<th>Nama Kriteria</th>
				<th>Simbol</th>
			</tr>
			<?php
				include"../db/koneksi.php";
				$sql=mysql_query("SELECT * FROM tblkriteria");
				$no=0;
				while($data=mysql_fetch_array($sql))
				{
					$no++;
					
			?>
			<tr bgcolor="#FFF">
				<td align="center"><input type="checkbox" name="id_k[]" value="<?php echo $data['idkriteria']?>"></td>
				<td>
					<input type="hidden" name="idkriteria[]" value="<?php echo $data['idkriteria']?>">
					<input class="isi" type="text" name="nama_k[]" value="<?php echo $data['nama_kriteria']?>" style="border:none;width:500px;background-color:none">
				</td>
				<td align="center"><?php echo $data['simbol']?></td>
			</tr>
			<?php
				}
			?>
		</tbody>
	</table>
	<div>
		<input type="hidden" name="txtjumlah" value="<?php echo $no?>">
		<button type="submit" name="btnproses" value="simpan_perubahan_kriteria" class="btn btn-success" data-rel="tooltip" title="Klik Untuk Simpan Perubahan Kriteria"><i class="icon-share icon-white"></i> <b>Simpan Perubahan Kriteria</b></button>
		<button type="submit" name="btnproses" value="hapus_kriteria" class="btn btn-danger" title="Klik Untuk Hapus Data Kriteria" onClick="return confirm('Apakah Data Kriteria ini Akan Dihapus ??');"><i class="icon-remove icon-white"></i> <b>Hapus Kriteria</b></button>
	</div>
</form>
<!--melakukan pembobotan pada kriteria-->
<form name="fbobot_k" method="post" action="proses.php">
	<table class="table table-bordered">
		<tr>
			<td>X</td>
			<?php
				$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
				while($data_simbol = mysql_fetch_array($sql_simbol))
				{
			?>
			<td><?php echo $data_simbol['simbol']?></td>
			<?php
				}
			?>
		</tr>
		<?php
			$sql = mysql_query("SELECT * FROM tblkriteria ORDER BY idkriteria ASC");
			$ke=0;
			while($data = mysql_fetch_array($sql))
			{
				$ke++;
		?>
		<tr>
			<td>
				<?php echo $data['simbol']?>
				<input type="hidden" name="id_k[]" value="<?php echo $data['idkriteria']?>">
				<input type="hidden" name="simbol_k[]" value="<?php echo $data['simbol']?>">
			</td>
			<?php
				$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
				while($data_simbol = mysql_fetch_array($sql_simbol))
				{
			?>
			<td>
				<select name="<?php echo $data_simbol['simbol'].$ke?>" style="width:70px">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="<?php echo round((1/2),3)?>">1/2</option>
					<option value="<?php echo round((1/3),3)?>">1/3</option>
					<option value="<?php echo round((1/4),3)?>">1/4</option>
					<option value="<?php echo round((1/5),3)?>">1/5</option>
					<option value="<?php echo round((1/6),3)?>">1/6</option>
					<option value="<?php echo round((1/7),3)?>">1/7</option>
					<option value="<?php echo round((1/8),3)?>">1/8</option>
					<option value="<?php echo round((1/9),3)?>">1/9</option>
				</select>
			</td>
			<?php
				}
			?>
		</tr>
		<?php
			}
		?>
	</table>
	<div>
		<input type="hidden" value="<?php echo $ke?>" name="jumlah">
		<button type="submit" name="btnproses" value="simpan_bobot_kriteria" class="btn btn-success" data-rel="tooltip" title="Klik Untuk Simpan Bobot Kriteria"><i class="icon-edit icon-white"></i> <b>Simpan Bobot Kriteria</b></button>
	</div>
</form>
<!--Data bobot mentah-->
<h2>Data Bobot Kriteria</h2>
<table class="table table-bordered">
	<tr>
		<td>X</td>
		<?php
			$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY simbol ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td><?php echo $data_simbol['simbol']?></td>
		<?php
			}
		?>
	</tr>
	<?php
		$sql = mysql_query("SELECT * FROM tblbobotkriteria ORDER BY simbol ASC");
		$ke=0;
		while($data = mysql_fetch_array($sql))
		{
			$ke++;
	?>
	<tr>
		<td><?php echo $data['simbol']?></td>
		<?php
			$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td><?php echo $data[$data_simbol['simbol']]?></td>
		<?php
			}
		?>
	</tr>
	<?php
		}
	?>
</table>
<h2>Data Normalisasi Bobot Kriteria</h2>
<table class="table table-bordered">
	<tr>
		<td>X</td>
		<?php
			$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td><?php echo $data_simbol['simbol']?></td>
		<?php
			}
		?>
		<td>Rata-Rata</td>
	</tr>
	<?php
		$sql = mysql_query("SELECT * FROM tblnormalisasikriteria ORDER BY idkriteria ASC");
		$ke=0;
		while($data = mysql_fetch_array($sql))
		{
			$ke++;
	?>
	<tr>
		<td><?php echo $data['simbol']?></td>
		<?php
			$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
		?>
		<td><?php echo $data[$data_simbol['simbol']]?></td>
		<?php
			}
		?>
		<td><?php echo $data['rata2']?></td>
	</tr>
	<?php
		}
	?>
	<tr>
		<td colspan="<?php echo $ke+1?>"><b style="color:red">Jumlah Rata-Rata Bobot</b></td>
		<td>
			<?php
				$sql_simbol = mysql_query("SELECT sum(rata2) as jumlah FROM tblnormalisasikriteria");
				if($data_simbol = mysql_fetch_array($sql_simbol))
				{
					echo "<b style=color:red>".round($data_simbol['jumlah'],2).".00</b>";
				}
			?>
		</td>
	</tr>
</table>
<?php
	}
	else
	{
		include"error_page.php";
	}
?>