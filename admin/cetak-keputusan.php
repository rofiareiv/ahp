<?php
	include"../db/koneksi.php";
?>
<html lang="en">
<head>
<title>Cetak Hasil Keputusan</title>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
<link href="images/icon.png" rel="shortcut icon" type="image/png">
<style type="text/css" media="screen,print">
*{
	margin:0;
	padding:0;
}
body{
	background-color:#333333;
	font:normal 8pt/100% tahoma,arial,sans-serif;
}
div.page{
	background-color:white;
	color:black;
	min-height:auto;
	margin:1cm auto;
	padding: 0.5cm 1cm 1cm 1cm;
	width:23.5cm;/*untuk lebar halaman*/
}
div.header{
	background-color:white;
	border-bottom:3px solid black;
	font-family:"Times New Roman",serif;	
	padding-bottom:.5cm;
	text-align:center;
	color:black;
	padding-top:20px;
}
div.header2{
	text-align:center;
	font-size:13pt;
	font-weight:bold;
	line-height:120%;
	padding-top:0.2cm;
}
div.header div.h1{
	font-size:15pt;
	font-weight:bold;
	line-height:18pt;
	font-family:Tahoma,Calibri;
}
div.header div.h2{
	font-size:13pt;	
	line-height:18pt;
	font-family:Calibri;	
}
div.header div.h3{
	font-size:13pt;	
	line-height:12pt;	
}
div.content{
	margin-top:.2cm;
}
table.data{	
	border-collapse:collapse;
	width:100%;
	font-family:Cambria,Calibri;
	font-size:14px;
}
th{
	padding:3px;
	border:1px solid black;
}
table.data td{
	padding:4px;
}
span.pilihan{
	border-bottom:1px dotted black;
}
</style>
<style type="text/css" media="print">
body{
	background-color:white;
}
div.page{
	margin:auto;
	min-height:29.5cm;
	margin:auto;
	padding:0 1cm;	
	width:21.5cm;
}
div.header{		
	padding-bottom:.5cm;
}
</style>
</head>
<body onload="window.print();">
	<div class="page">
		<div class="header">
			<img style="float:left;margin-top:-20px;margin-right:-40px" src="images/uim.png" alt="Unira" width="100" height="95">
			<!--
			<img style="float: right;margin-top:-12px" src="images/sehat.png" width="60" height="60">		
			-->
			<div class="h1">APLIKASI SISTEM PENDUKUNG KEPUTUSAN TOKO</div>			
			<div class="h1">MENGGUNAKAN METODE ANALITIC HIRARCY PROSES</div>
			<div class="h1">UNIVERSITAS ISLAM MADURA</div>
		</div>
		<div class="content">
			<h2>Data Bobot Kriteria</h2><br>
			<table style="border-collapse:collapse;border:1px solid;" border="1" width="100%" cellpadding="5">
				<th>X</th>
					<?php
						$sql_simbol = mysqli_query($conn, "SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
						while($data_simbol = mysqli_fetch_array($sql_simbol))
						{
					?>
				<th><?php echo $data_simbol['simbol']?></th>
					<?php
						}
					?>
				<?php
					$sql = mysqli_query($conn, "SELECT * FROM tblbobotkriteria ORDER BY idkriteria ASC");
					$ke=0;
					while($data = mysqli_fetch_array($sql))
					{
						$ke++;
				?>
				<tr>
					<td align="center"><?php echo $data['simbol']?></td>
					<?php
						$sql_simbol = mysqli_query($conn, "SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
						while($data_simbol = mysqli_fetch_array($sql_simbol))
						{
					?>
					<td align="center"><?php echo $data[$data_simbol['simbol']]?></td>
					<?php
						}
					?>
				</tr>
				<?php
					}
				?>
			</table>
			<br>
			<h2>Data Normalisasi Bobot Kriteria</h2><br>
			<table style="border-collapse:collapse;border:1px solid" border="1" width="100%" cellpadding="5">
				<th>X</th>
					<?php
						$sql_simbol = mysqli_query($conn, "SELECT simbol FROM tblkriteria ORDER BY simbol ASC");
						while($data_simbol = mysqli_fetch_array($sql_simbol))
						{
					?>
				<th><?php echo $data_simbol['simbol']?></th>
					<?php
						}
					?>
				<th>Rata-Rata</th>
				<?php
					$sql = mysqli_query($conn, "SELECT * FROM tblnormalisasikriteria ORDER BY simbol ASC");
					$ke=0;
					while($data = mysqli_fetch_array($sql))
					{
						$ke++;
				?>
				<tr>
					<td align="center"><?php echo $data['simbol']?></td>
					<?php
						$sql_simbol = mysqli_query($conn, "SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
						while($data_simbol = mysqli_fetch_array($sql_simbol))
						{
					?>
					<td align="center"><?php echo $data[$data_simbol['simbol']]?></td>
					<?php
						}
					?>
					<td align="center"><?php echo $data['rata2']?></td>
				</tr>
				<?php
					}
				?>
				<tr>
					<td colspan="<?php echo $ke+1?>"><b style="color:red">Jumlah Rata-Rata Bobot</b></td>
					<td>
						<?php
							$sql_simbol = mysqli_query($conn, "SELECT sum(rata2) as jumlah FROM tblnormalisasikriteria");
							if($data_simbol = mysqli_fetch_array($sql_simbol))
							{
								echo "<b style=color:red>".round($data_simbol['jumlah'],2).".00</b>";
							}
						?>
					</td>
				</tr>
			</table>
			<br>
			<!--Data Alternatif-->
			<?php
				$sql_jumlah = mysqli_query($conn, "SELECT COUNT(idkriteria) as jumlah FROM tblkriteria");
				$data_jumlah = mysqli_fetch_array($sql_jumlah);
				$jumlah_k = $data_jumlah['jumlah'];
				for($tanda=1; $tanda<=$jumlah_k; $tanda++)
				{
			?>
			<h2>Nilai Bobot Perbandingan Alternatif dengan Kriteria Ke-<?php echo $tanda?></h2><br>
			<table style="border-collapse:collapse;border:1px solid" border="1" width="100%" cellpadding="5">
				<th>X</th>
					<?php
						$sql_simbol = mysqli_query($conn, "SELECT nama_alias FROM tblalternatif ORDER BY idalternatif ASC");
						while($data_simbol = mysqli_fetch_array($sql_simbol))
						{
					?>
				<th><?php echo $data_simbol['nama_alias']?></th>
					<?php
						}
					?>
				<?php
					$sql = mysqli_query($conn, "SELECT * FROM tblbobotalternatif WHERE tanda='$tanda' ORDER BY idalternatif ASC");
					$ke=0;
					while($data = mysqli_fetch_array($sql))
					{
						$ke++;
				?>
				<tr>
					<td align="center"><?php echo $data['nama_alias']?></td>
					<?php
						$sql_simbol = mysqli_query($conn, "SELECT nama_alias FROM tblalternatif ORDER BY idAlternatif ASC");
						while($data_simbol = mysqli_fetch_array($sql_simbol))
						{
					?>
					<td align="center"><?php echo $data[$data_simbol['nama_alias']]?></td>
					<?php
						}
					?>
				</tr>
				<?php
					}
				?>
			</table>
			<br>
			<h2>Menghitung Normalisasi (Eigen Value) Bobot Perbadingan Alternatif dengan Kriteria Ke-<?php echo $tanda?></h2><br>
			<table style="border-collapse:collapse;border:1px solid" border="1" width="100%" cellpadding="5">
				<th>X</th>
					<?php
						$sql_simbol = mysqli_query($conn, "SELECT nama_alias FROM tblalternatif ORDER BY idAlternatif ASC");
						while($data_simbol = mysqli_fetch_array($sql_simbol))
						{
					?>
				<th><?php echo $data_simbol['nama_alias']?></th>
					<?php
						}
					?>
				<th>Rata-Rata</th>
				<?php
					$sql = mysqli_query($conn, "SELECT * FROM tblnormalisasialternatif WHERE tanda='$tanda' ORDER BY idalternatif ASC");
					$ke=0;
					while($data = mysqli_fetch_array($sql))
					{
						$ke++;
				?>
				<tr>
					<td align="center"><?php echo $data['nama_alias']?></td>
					<?php
						$sql_simbol = mysqli_query($conn, "SELECT nama_alias FROM tblalternatif ORDER BY idAlternatif ASC");
						while($data_simbol = mysqli_fetch_array($sql_simbol))
						{
					?>
					<td align="center"><?php echo $data[$data_simbol['nama_alias']]?></td>
					<?php
						}
					?>
					<td align="center"><?php echo $data['rata2']?></td>
				</tr>
				<?php
					}
				?>
				<tr>
					<td colspan="<?php echo $ke+1?>"><b style="color:red">Jumlah Rata-Rata Bobot</b></td>
					<td>
						<?php
							$sql_simbol = mysqli_query($conn, "SELECT sum(rata2) as jumlah FROM tblnormalisasialternatif WHERE tanda='$tanda'");
							if($data_simbol = mysqli_fetch_array($sql_simbol))
							{
								echo "<b style=color:red>".round($data_simbol['jumlah'],2).".00</b>";
							}
						?>
					</td>
				</tr>
			</table>
			<br><br>
			<?php
				}
			?>
			<h2>Matriks Normalisasi (Eigen Value) Kriteria</h2><br>
			<table style="border-collapse:collapse;border:1px solid" border="1" width="30%" cellpadding="5">
				<th>No</th>
				<th>Simbol</th>
				<th>Nilai</th>
				<?php
					$sql_rata = mysqli_query($conn, "SELECT * FROM tblnormalisasikriteria ORDER BY idkriteria ASC");
					$no=0;
					while($data = mysqli_fetch_array($sql_rata))
					{
						$no++;
				?>
				<tr>
					<td align="center"><?php echo $no?></td>
					<td align="center"><?php echo $data['simbol']?></td>
					<td align="center"><?php echo $data['rata2']?></td>
				</tr>
				<?php
					}
				?>
			</table>
			<br>
			<h3>Matriks Normalisasi (Eigen Value) Alternatif</h3>
			<table style="border-collapse:collapse;border:1px solid" border="1" width="70%" cellpadding="5">
				<th>No</th>
				<th>Nama Alternatif</th>
				<?php
					$sql_simbol = mysqli_query($conn, "SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
					while($data_simbol = mysqli_fetch_array($sql_simbol))
					{
				?>
				<th><?php echo $data_simbol['simbol']?></th>
				<?php
					}
				?>
				</tr>
				<?php
					$sql_rata = mysqli_query($conn, "SELECT * FROM tblrataalternatif ORDER BY idalternatif ASC");
					$no=0;
					while($data = mysqli_fetch_array($sql_rata))
					{
						$no++;
				?>
				<tr>
					<td align="center"><?php echo $no?></td>
					<td align="center"><?php echo $data['nama_alternatif']?></td>
					<?php
						$sql_simbol = mysqli_query($conn, "SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
						while($data_simbol = mysqli_fetch_array($sql_simbol))
						{
					?>
					<td align="center"><?php echo $data[$data_simbol['simbol']]?></td>
					<?php
						}
					?>
				</tr>
				<?php
					}
				?>
			</table>
			<br>
			<h3>Hasil Keputusan (Eigen Vector)</h3>
			<table style="border-collapse:collapse;border:1px solid" border="1" width="40%" cellpadding="5">
				<th>No</th>
				<th>Nama Alternatif</th>
				<th>Nilai</th>
				<?php
					$sql_rata = mysqli_query($conn, "SELECT * FROM tblkeputusan ORDER BY nilai DESC");
					$no=0;
					while($data = mysqli_fetch_array($sql_rata))
					{
						$no++;
				?>
				<tr>
					<td align="center"><?php echo $no?></td>
					<td><?php echo $data['nama_alternatif']?></td>
					<td align="center"><?php echo $data['nilai']?></td>
				</tr>
				<?php
					}
				?>
			</table>
			<br/>
			<p style="color:red;font-weight:bold;font-size:18px">Keterangan : Kandidat yang memperolah nilai terbesar menjadi alternatif terbaik</p>
		</div>
	</div>
</body>
</html>
