<?php
	session_start();
	if(!empty($_SESSION['iduser']) && !empty($_SESSION['username']) && !empty($_SESSION['password']))
	{
		include"../db/koneksi.php";
		//membaut tabel bobot rata2 alternatif
		$query1="CREATE TABLE IF NOT EXISTS tblrataalternatif (idrata int(30) NOT NULL AUTO_INCREMENT,idalternatif int(30) NOT NULL,nama_alternatif varchar(100) NOT NULL,";
		$query2="CREATE TABLE IF NOT EXISTS tblkeputusan (idkeputusan int(30) NOT NULL AUTO_INCREMENT,idalternatif int(30) NOT NULL,nama_alternatif varchar(100) NOT NULL,nilai double NOT NULL,";
		
		$sql_jumlah = mysql_query("SELECT COUNT(idkriteria) AS jumlah FROM tblkriteria");
		if($jumlahnya = mysql_fetch_array($sql_jumlah))
		{
			$jumlah = $jumlahnya['jumlah'];
		}
		$sql = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
		$ke=0;
		while($data_simbol = mysql_fetch_array($sql))
		{
			$ke++;
			if($ke<$jumlah)
			{
				$query1.=$data_simbol['simbol']." double NOT NULL,";
			}
			else
			{
				$query1.=$data_simbol['simbol']." double NOT NULL,";
			}
		}
		$query1.=$data_simbol['simbol']." PRIMARY KEY (idrata)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
		$query2.=" PRIMARY KEY (idkeputusan)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
		$buat  = mysql_query($query1);	
		$buat  = mysql_query($query2);
		
		//kosongkan data terlebih dahulu
		$kosong = mysql_query("TRUNCATE TABLE tblrataalternatif");
		$kosong = mysql_query("TRUNCATE TABLE tblkeputusan");
		//=============================mengisi tabel rata alternatif============================
		//hitung jumlah kriteria
		$sql_jumlah = mysql_query("SELECT COUNT(idkriteria) as jumlah FROM tblkriteria");
		$data_jumlah = mysql_fetch_array($sql_jumlah);
		$jumlah_k = $data_jumlah['jumlah'];
		$sql = mysql_query("SELECT idalternatif,nama_alias FROM tblalternatif ORDER BY idalternatif ASC");
		while($data = mysql_fetch_array($sql))
		{
			$data_nilai="";
			$idalternatif = $data['idalternatif'];
			$alias = $data['nama_alias'];
			//ambil semua nilai rata per alternatif
			$sql_rata = mysql_query("SELECT rata2 FROM tblnormalisasialternatif WHERE idalternatif='$idalternatif' AND nama_alias='$alias'");
			$ke=0;
			while($data_rata = mysql_fetch_array($sql_rata))
			{
				$ke++;
				$rata2 = $data_rata['rata2'];
				if($ke<$jumlah_k)
					$data_nilai.="'".$rata2."',";
				else
					$data_nilai.="'".$rata2."'";
			}
			//simpan nilai rata
			$simpan = mysql_query("INSERT INTO tblrataalternatif VALUES(NULL,'$idalternatif','$alias',$data_nilai)");
		}
		//===========================mengisi tabel keputusan==========================================
		//ambil data alternatif
		$sql_a = mysql_query("SELECT * FROM tblalternatif ORDER BY idalternatif ASC");
		while($data_a = mysql_fetch_array($sql_a))
		{
			$idalternatif = $data_a['idalternatif'];
			$nama_alternatif = $data_a['nama_alternatif'];
			$nama_alias = $data_a['nama_alias'];
			$sql_simbol = mysql_query("SELECT simbol FROM tblkriteria ORDER BY idkriteria ASC");
			$total=0;
			while($data_simbol = mysql_fetch_array($sql_simbol))
			{
				$nilai_a=0;
				$nilai_k=0;
				$simbol = $data_simbol['simbol'];
				//ambil nilai pada tabel rata alternatif sesuai alias A1,A2,A3,A4,A5,A6
				$sql_nilai = mysql_query("SELECT $simbol FROM tblrataalternatif WHERE idalternatif='$idalternatif' AND nama_alternatif='$nama_alias'");
				$data_nilai = mysql_fetch_array($sql_nilai);
				$nilai_a = $data_nilai[$simbol];
				//ambil nilai pada tabel rata kriteria sesuai alias
				$sql_nilai = mysql_query("SELECT rata2 FROM tblnormalisasikriteria WHERE simbol='$simbol'");
				$data_nilai = mysql_fetch_array($sql_nilai);
				$nilai_k = $data_nilai['rata2'];
				//kalikan
				$kali = round(($nilai_a * $nilai_k),3);
				$total+=$kali;
			}
			//simpan ke tabel keputusan
			$sql_simpan = mysql_query("INSERT INTO tblkeputusan VALUES(NULL,'$idalternatif','$nama_alternatif','$total')");
		}
		echo "<script>alert('Proses Perhitungan Metode AHP Selesai..!');location.href=\"home.php?module=".md5("keputusan")."\";</script>";
	}
	else
		echo "<script>alert('Maaf Anda Harus Login Untuk Dapat Melakukan Proses AHP..!');location.href=\"../index.php\";</script>";
?>