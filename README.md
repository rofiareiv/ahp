## Create a new repository
```
git clone https://gitlab.com/rofiareiv/ahp.git
cd ahp
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
## Push an existing folder
```
cd existing_folder
git init
git remote add origin https://gitlab.com/rofiareiv/ahp.git
git add .
git commit -m "Initial commit"
git push -u origin master
```