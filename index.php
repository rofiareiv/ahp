<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
        <title>:: Login Administrator ::</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
        <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="images/favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/style2.css" />
		<link rel="stylesheet" type="text/css" href="css/animate-custom.css" />
    </head>
    <body  onload="waktu()">
        <div class="container">
            <!-- Codrops top bar -->
            <div class="codrops-top">
                <div class="clr"></div>
            </div><!--/ Codrops top bar -->
            <header>
                
            </header>
            <section>				
                <div id="container_demo" >
                    <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                        <div id="login" class="animate form">
                            <form  action="cek_login.php" autocomplete="on" method="post"> 
                                <h1>Login Administrator</h1> 
                                <p> 
                                    <label for="username" class="uname" data-icon="u" > Nama Anda </label>
                                    <input id="username" name="txtusername" required="" type="text" placeholder="Nama Anda" autofocus required/>
                                </p>
                                <p> 
                                    <label for="password" class="youpasswd" data-icon="p"> Password Anda </label>
                                    <input id="password" name="txtpassword" required="required" type="password" placeholder="Password Anda " /> 
                                </p>
								<p class="keeplogin"> 
									&nbsp;
								</p>
                                <p class="login button"> 
									
                                    <input type="submit" value="Login" /> 
								</p>
                                <p class="change_link">
									<div id="jam" style="margin-top:-70px;"></div>
									<div id="jam-nya"></div>
									<?php
										date_default_timezone_set('Asia/Jakarta'); // PHP 6 mengharuskan penyebutan timezone.
										$seminggu = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
										$hari = date("w");
										$hari_ini = $seminggu[$hari];
										$namabulan="";
										$tgl_sekarang = date("Ymd");
										$tgl  = date("d");
										$bln  = date("m");
										$thn  = date("Y");
										//$jam_sekarang = date("H:i");
										switch ($bln)
										{
											case 1: 
												$namabulan = "Januari";
												break;
											case 2:
												$namabulan = "Februari";
												break;
											case 3:
												$namabulan = "Maret";
												break;
											case 4:
												$namabulan = "April";
												break;
											case 5:
												$namabulan = "Mei";
												break;
											case 6:
												$namabulan = "Juni";
												break;
											case 7:
												$namabulan = "Juli";
												break;
											case 8:
												$namabulan = "Agustus";
												break;
											case 9:
												$namabulan = "September";
												break;
											case 10:
												$namabulan = "Oktober";
												break;
											case 11:
												$namabulan = "November";
												break;
											case 12:
												$namabulan = "Desember";
												break;
										}
										echo $hari_ini.",".$tgl."/".$namabulan."/".$thn;
									?>
								</p>
                            </form>
                        </div>
						
                    </div>
                </div>  
            </section>
        </div>
<script type="text/javascript">
function waktu() 
{
	var tanggal = new Date();//mengambil format jam dari sistem
	setTimeout("waktu()");//memanggil fungsi waktu()
	//menuliskan output jam dengan format Jam : Menit : Detik
	document.getElementById("jam").innerHTML = tanggal.getHours()+":"+tanggal.getMinutes()+":"+tanggal.getSeconds();
	//document.getElementById("jam-nya").innerHTML = "<input name=jam type=hidden value="+tanggal.getHours()+">";
}
</script>
</body>
</html>